// GLView.h : CGLView ��Ľӿ�
//


#pragma once

namespace engine{
	class IAppHandler;
}
namespace graphic{
	class Device;
}

class CGLView : public CWnd
{
public:
	void onFrame();
private:
	graphic::Device* m_pDevice;

	void OnResize(int _nWidth, int _nHeight);

	void onDraggingL(int _x, int _y, int _nFlags);
	void onDraggingR(int _x, int _y, int _nFlags);
	void onDraggingM(int _x, int _y, int _nFlags);

	void onStartDragL(int _x, int _y, int _nFlags);
	void onStartDragR(int _x, int _y, int _nFlags);
	void onStartDragM(int _x, int _y, int _nFlags);

	void onStopDragL(int _x, int _y, int _nFlags);	
	void onStopDragR(int _x, int _y, int _nFlags);
	void onStopDragM(int _x, int _y, int _nFlags);

	void onCursorMove(int _x, int _y, int _nFlags);

	bool m_bMouseDownL;
	bool m_bMouseDownR;
	bool m_bMouseDownM;
	bool m_bStartDragL;	
	bool m_bStartDragR;
	bool m_bStartDragM;

	/*
	void SetCursor(int x_,	int y_);
	void GetCursor(int& x_,	int& y_);
	*/
	int	m_ViewSize[2];
	

protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	DECLARE_MESSAGE_MAP()
public:
	CGLView();
	virtual ~CGLView();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};

