// GLFrm.h : CGLFrm 类的接口
//


#pragma once

#include "GLView.h"

class CGLFrm : public CFrameWnd
{
public:
	CGLFrm();
	void calculate_outter_size();
	bool onFrame();

protected: 
	DECLARE_DYNAMIC(CGLFrm)
// 重写
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);

// 实现
public:
	virtual ~CGLFrm();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 控件条嵌入成员
	CToolBar    m_wndToolBar;
	CGLView		m_wndView;

	int m_view_outter_width;
	int m_view_outter_height;
	int m_main_outter_width;
	int m_main_outter_height;
	bool b_calculate_size;

// 生成的消息映射函数
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	afx_msg void OnActivateApp(BOOL bActive, DWORD dwThreadID);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
};


