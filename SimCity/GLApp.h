// SimCity.h : SimCity 应用程序的主头文件
//
#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"       // 主符号
#include "handler.h"

// CGLApp:
// 有关此类的实现，请参阅 SimCity.cpp
//
class CGLFrm;
class CGLApp : public CWinApp
{
public:
	CGLApp();

	void onReleaseMainFrame();
	engine::IAppHandler* getAppHandler();

private:
	typedef void (*MAIN_LOOP_FUNC)(void* _pFunc, CGLFrm* _pFrame);
	MAIN_LOOP_FUNC m_pMainLoopFunc;
	engine::IAppHandler*	m_pAppHandler;


// 重写
public:
	virtual BOOL	InitInstance();
	virtual int		ExitInstance();
	virtual int		Run();
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()	
	
};

extern CGLApp theApp;