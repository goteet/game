// GLView.cpp : CGLView 类的实现
//

#include "stdafx.h"
#include "GLApp.h"
#include "GLView.h"


#include "device.h"
#include "engine/context.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static CGLView* s_view = NULL;

CGLView::CGLView()
{
}

CGLView::~CGLView()
{
	if(s_view == this)
	{
		s_view = NULL;
	}
}


BEGIN_MESSAGE_MAP(CGLView, CWnd)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEWHEEL()
	ON_WM_MOUSEMOVE()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_MBUTTONDBLCLK()
	ON_WM_SIZE()
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()


void engine::SetCursorPos(int _x, int _y)
{
	if(s_view)
	{
		POINT point;
		point.x = _x;
		point.y = _y;
		s_view->ClientToScreen(&point);
		::SetCursorPos(point.x, point.y);
	}
}
// CGLView 消息处理程序

BOOL CGLView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

int CGLView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	graphic::DeviceConfig conf;
	conf.Window = this->GetSafeHwnd();
	conf.SetWindowVisual = 0;

	m_pDevice = graphic::CreateDevice(conf);
	if(!m_pDevice)
		return -1;
	else
		engine::AppContext::instance()->pDevice = m_pDevice;

	s_view = this;
	return 0;

}

void CGLView::OnDestroy()
{
	m_pDevice->Release();
	m_pDevice = 0;
	CWnd::OnDestroy();
	// TODO: Add your message handler code here
}

//////////////////////////////////////////////////////////////////////////
void CGLView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	m_bMouseDownL = true;
	//CWnd::OnLButtonDown(nFlags, point);
}

void CGLView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	CWnd::OnLButtonDblClk(nFlags, point);
}

void CGLView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if(m_bMouseDownL)
	{
		if(m_bStartDragL)
		{
			onStopDragL(point.x, point.y, nFlags);
			m_bStartDragL = false;
		}
	}
	m_bMouseDownL = false;
	//CWnd::OnLButtonUp(nFlags, point);
}
//////////////////////////////////////////////////////////////////////////
void CGLView::OnRButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	CWnd::OnRButtonDblClk(nFlags, point);
}

void CGLView::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	m_bMouseDownR = true;
	//CWnd::OnRButtonDown(nFlags, point);
}

void CGLView::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if(m_bMouseDownR)
	{
		if(m_bStartDragR)
		{
			onStopDragR(point.x, point.y, nFlags);
			m_bStartDragR = false;
		}
	}
	m_bMouseDownR = false;
	//CWnd::OnRButtonUp(nFlags, point);
}
//////////////////////////////////////////////////////////////////////////
BOOL CGLView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: Add your message handler code here and/or call default

	return CWnd::OnMouseWheel(nFlags, zDelta, pt);
}

void CGLView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	int x = point.x;
	int y = point.y;

	static bool s_any_dragging = false;
	if(m_bMouseDownL)
	{
		if(!m_bStartDragL)
		{
			onStartDragL(x,y,nFlags);
			m_bStartDragL = true;
		}
		else
		{
			onDraggingL(x,y,nFlags);
		}
		s_any_dragging = true;
	}

	if(m_bMouseDownR)
	{
		if(!m_bStartDragR)
		{
			onStartDragR(x,y,nFlags);
			m_bStartDragL = true;
		}
		else
		{
			onDraggingR(x,y,nFlags);
		}
		s_any_dragging = true;
	}

	if(m_bMouseDownM)
	{
		if(!m_bStartDragM)
		{
			onStartDragM(x,y,nFlags);
			m_bStartDragL = true;
		}
		else
		{
			onDraggingM(x,y,nFlags);
		}
		s_any_dragging = true;
	}

	if(!s_any_dragging)
		onCursorMove(x,y,nFlags);
	else
		s_any_dragging = false;
	CWnd::OnMouseMove(nFlags, point);
}
void CGLView::onCursorMove( int _x, int _y, int _nFlags )
{}
//////////////////////////////////////////////////////////////////////////
void CGLView::OnMButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	m_bMouseDownM = true;
	//CWnd::OnMButtonDown(nFlags, point);
}

void CGLView::OnMButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if(m_bMouseDownM)
	{
		if(m_bStartDragM)
		{
			onStopDragM(point.x, point.y, nFlags);
			m_bStartDragM = false;
		}
	}
	m_bMouseDownM = false;
	//CWnd::OnMButtonUp(nFlags, point);
}

void CGLView::OnMButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	CWnd::OnMButtonDblClk(nFlags, point);
}
void CGLView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);
	OnResize(cx,cy);
	// TODO: Add your message handler code here
}

//////////////////////////////////////////////////////////////////////////
void CGLView::OnResize( int _nWidth, int _nHeight )
{
	m_pDevice->SetViewport(_nWidth, _nHeight);
	if(theApp.getAppHandler())
		theApp.getAppHandler()->onResize(_nWidth, _nHeight);
	m_ViewSize[0] = _nWidth;
	m_ViewSize[1] = _nHeight;
}
void CGLView::onDraggingL( int _x, int _y, int _nFlags )
{
	if(theApp.getAppHandler())
		theApp.getAppHandler()->onDraggingL(_x,_y,
		_nFlags & MK_CONTROL,
		_nFlags & MK_SHIFT,
		_nFlags & MK_ALT
		);
}

void CGLView::onDraggingR( int _x, int _y, int _nFlags )
{
	if(theApp.getAppHandler())
		theApp.getAppHandler()->onDraggingR(_x,_y,
		_nFlags & MK_CONTROL,
		_nFlags & MK_SHIFT,
		_nFlags & MK_ALT
		);
}

void CGLView::onDraggingM( int _x, int _y, int _nFlags )
{
	if(theApp.getAppHandler())
		theApp.getAppHandler()->onDraggingM(_x,_y,
		_nFlags & MK_CONTROL,
		_nFlags & MK_SHIFT,
		_nFlags & MK_ALT
		);
}

void CGLView::onStartDragL( int _x, int _y, int _nFlags )
{
	if(theApp.getAppHandler())
		theApp.getAppHandler()->onDragBeginL(_x,_y,
		_nFlags & MK_CONTROL,
		_nFlags & MK_SHIFT,
		_nFlags & MK_ALT
		);
}

void CGLView::onStartDragR( int _x, int _y, int _nFlags )
{
	if(theApp.getAppHandler())
		theApp.getAppHandler()->onDragBeginR(_x,_y,
		_nFlags & MK_CONTROL,
		_nFlags & MK_SHIFT,
		_nFlags & MK_ALT
		);
}

void CGLView::onStartDragM( int _x, int _y, int _nFlags )
{
	if(theApp.getAppHandler())
		theApp.getAppHandler()->onDragBeginM(_x,_y,
		_nFlags & MK_CONTROL,
		_nFlags & MK_SHIFT,
		_nFlags & MK_ALT
		);
}

void CGLView::onStopDragL( int _x, int _y, int _nFlags )
{
	if(theApp.getAppHandler())
		theApp.getAppHandler()->onDragEndL(_x,_y,
		_nFlags & MK_CONTROL,
		_nFlags & MK_SHIFT,
		_nFlags & MK_ALT
		);
}

void CGLView::onStopDragR( int _x, int _y, int _nFlags )
{
	if(theApp.getAppHandler())
		theApp.getAppHandler()->onDragEndR(_x,_y,
		_nFlags & MK_CONTROL,
		_nFlags & MK_SHIFT,
		_nFlags & MK_ALT
		);
}

void CGLView::onStopDragM( int _x, int _y, int _nFlags )
{
	if(theApp.getAppHandler())
		theApp.getAppHandler()->onDragEndM(_x,_y,
		_nFlags & MK_CONTROL,
		_nFlags & MK_SHIFT,
		_nFlags & MK_ALT
		);
}

//////////////////////////////////////////////////////////////////////////
void CGLView::onFrame()
{
	//m_pDevice->SetBkColor(0,0,1,1);
	m_pDevice->Clear(true,true,false);
	theApp.getAppHandler()->onRender();
	m_pDevice->Present();
}
void CGLView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT _nFlags)
{
	// TODO: Add your message handler code here and/or call default

	CWnd::OnKeyDown(nChar, nRepCnt, _nFlags);
	int char_conv = nChar;
	switch(nChar)
	{
		case VK_LEFT:	char_conv= engine::IInputHandler::ARROW_L;break;
		case VK_UP:		char_conv= engine::IInputHandler::ARROW_U;break;
		case VK_RIGHT:	char_conv= engine::IInputHandler::ARROW_R;break;
		case VK_DOWN:	char_conv= engine::IInputHandler::ARROW_D;break;
		default:
			if(nChar >= 'A' && nChar <= 'Z')
				char_conv = engine::IInputHandler::LETTER_A + (nChar-'A');
			else if(nChar >= '0' && nChar <= '9')
				char_conv = engine::IInputHandler::NUM_0+ (nChar-'0');
	}

	theApp.getAppHandler()->onKeyDown(
		char_conv,
		_nFlags & MK_CONTROL,
		_nFlags & MK_SHIFT,
		_nFlags & MK_ALT);
}
