// GLFrm.cpp : CGLFrm 类的实现
//

#include "stdafx.h"
#include "GLApp.h"

#include "GLFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CGLFrm

IMPLEMENT_DYNAMIC(CGLFrm, CFrameWnd)

BEGIN_MESSAGE_MAP(CGLFrm, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_ACTIVATEAPP()
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()


// CGLFrm 构造/析构

CGLFrm::CGLFrm()
: b_calculate_size(false)
{
	// TODO: 在此添加成员初始化代码
}

CGLFrm::~CGLFrm()
{
	theApp.onReleaseMainFrame();
}


int CGLFrm::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	// 创建一个视图以占用框架的工作区
	if (!m_wndView.Create(NULL, NULL, WS_CHILD | WS_VISIBLE,
		CRect(0, 0, 0, 0), this, AFX_IDW_PANE_FIRST, NULL))
	{
		TRACE0("未能创建视图窗口\n");
		return -1;
	}
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("未能创建工具栏\n");
		return -1;      // 未能创建
	}

	// TODO: 如果不需要可停靠工具栏，则删除这三行
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	return 0;
}

BOOL CGLFrm::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.lpszClass = AfxRegisterWndClass(0);
	return TRUE;
}


// CGLFrm 诊断

#ifdef _DEBUG
void CGLFrm::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CGLFrm::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CGLFrm 消息处理程序

void CGLFrm::OnSetFocus(CWnd* /*pOldWnd*/)
{
	// 将焦点前移到视图窗口
	m_wndView.SetFocus();
}

BOOL CGLFrm::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
	// 让视图第一次尝试该命令
	if (m_wndView.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
		return TRUE;

	// 否则，执行默认处理
	return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

bool CGLFrm::onFrame()
{	
	engine::IAppHandler* p_app_handler = theApp.getAppHandler();
	if(!p_app_handler->onFrame())
		return false;
	

	m_wndView.onFrame();
	return true;
}



void CGLFrm::OnDestroy()
{
	engine::IAppHandler* p_app_handler = theApp.getAppHandler();
	p_app_handler->onDestroy();
	CFrameWnd::OnDestroy();

	// TODO: Add your message handler code here
}


void CGLFrm::OnActivateApp(BOOL bActive, DWORD dwThreadID)
{
	CFrameWnd::OnActivateApp(bActive, dwThreadID);
	engine::IAppHandler* p_app_handler = theApp.getAppHandler();
	if(!p_app_handler)
		return;

	if(bActive)
	{
		p_app_handler->onActivate();
	}
	else
	{
		p_app_handler->onDeactivate();
	}

	// TODO: Add your message handler code here
}

void CGLFrm::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: Add your message handler code here and/or call default
	const int iphone_width	= 640 / 2;
	const int iphone_height	= 960 / 2;
	if(b_calculate_size)
	{
		int main_frm_width	= iphone_width + m_main_outter_width +0;
		int main_frm_height	= iphone_height+ m_main_outter_height+0;

		int view_width	= iphone_width + m_view_outter_width;
		int view_height	= iphone_height+ m_view_outter_height;

		lpMMI->ptMaxTrackSize.x    = main_frm_width;
		lpMMI->ptMaxTrackSize.y    = main_frm_height;

		lpMMI->ptMinTrackSize.x    = main_frm_width;
		lpMMI->ptMinTrackSize.y    = main_frm_height;

		CRect rect_view_window;
		m_wndView.GetWindowRect(&rect_view_window);
		ScreenToClient(&rect_view_window);
		m_wndView.MoveWindow(
			rect_view_window.left, rect_view_window.top,
			view_width,view_height);
	}
	
	CFrameWnd::OnGetMinMaxInfo(lpMMI);
}

void CGLFrm::calculate_outter_size()
{
	CRect rect_toolbar;
	CRect rect_mainfrm;
	CRect rect_view_window;
	CRect rect_view_client;
	m_wndView.GetWindowRect(&rect_view_window);
	m_wndView.GetClientRect(&rect_view_client);

	GetWindowRect(&rect_mainfrm);
	m_main_outter_width		= rect_mainfrm.Width() - rect_view_client.Width();
	m_main_outter_height	= rect_mainfrm.Height() - rect_view_client.Height();
	m_view_outter_width		= rect_view_window.Width() - rect_view_client.Width();
	m_view_outter_height	= rect_view_window.Height() - rect_view_client.Height();

	b_calculate_size = true;
}
