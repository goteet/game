// stdafx.cpp : 只包括标准包含文件的源文件
// SimCity.pch 将作为预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"

void logWin32(const char* pMessage, va_list& vaList)
{
	static char s_log_text[2048];
	vsprintf(s_log_text, pMessage, vaList);
	OutputDebugStringA(s_log_text);
	OutputDebugStringA("\n");
}


extern void ILog_Error(const char* str, ...){
	va_list lVarArgs;
	va_start(lVarArgs, str);

    logWin32(str, lVarArgs);

	va_end(lVarArgs);

}
extern void ILog_Warn(const char* str, ...){
	va_list lVarArgs;
	va_start(lVarArgs, str);

    logWin32(str, lVarArgs);

	va_end(lVarArgs);

}
extern void ILog_Info(const char* str, ...){
	va_list lVarArgs;
	va_start(lVarArgs, str);

    logWin32(str, lVarArgs);

	va_end(lVarArgs);

}
extern void ILog_Debug(const char* str, ...){
	va_list lVarArgs;
	va_start(lVarArgs, str);

    logWin32(str, lVarArgs);

	va_end(lVarArgs);

}