// SimCity.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "GLApp.h"
#include "GLFrm.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CGLApp

BEGIN_MESSAGE_MAP(CGLApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, &CGLApp::OnAppAbout)
END_MESSAGE_MAP()


#include "SimCityHandler.h"
engine::IAppHandler* CreateAppHandler()
{
	return CreateSimCityHandler();
}
CGLApp::CGLApp()
: m_pMainLoopFunc(0)
, m_pAppHandler(0)
{
	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CGLApp 对象

CGLApp theApp;


// CGLApp 初始化

BOOL CGLApp::InitInstance()
{
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO: 应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));
	// 若要创建主窗口，此代码将创建新的框架窗口
	// 对象，然后将其设置为应用程序的主窗口对象
	CGLFrm* pFrame = new CGLFrm;
	if (!pFrame)
		return FALSE;
	m_pMainWnd = pFrame;

	
	// 创建并加载框架及其资源
	pFrame->LoadFrame(IDR_MAINFRAME,
		WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL,
		NULL);

	m_pAppHandler = CreateAppHandler();
	if(!m_pAppHandler)
		return FALSE;
	

	//（wei乱码）惟一的一个窗口已初始化，因此显示它并对其进行更新
	pFrame->ShowWindow(SW_SHOW);
	pFrame->UpdateWindow();
	// 仅当具有后缀时才调用 DragAcceptFiles
	//  在 SDI 应用程序中，这应在 ProcessShellCommand 之后发生

	
	if(!m_pAppHandler->onInitial())
		return FALSE;
	pFrame->calculate_outter_size();
	CRect dummy;
	pFrame->GetWindowRect(&dummy);
	pFrame->MoveWindow(dummy);
	return TRUE;

	
	

}
int CGLApp::ExitInstance()
{
	// TODO: Add your specialized code here and/or call the base class
	m_pAppHandler->Release();
	return CWinApp::ExitInstance();
}



// CGLApp 消息处理程序




// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// 用于运行对话框的应用程序命令
void CGLApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}


// CGLApp 消息处理程序

// CGLApp message handlers
namespace{
	void MainLoop_DoNothing(void* pFunc_, CGLFrm* _pFrame){}
	void MainLoop_RealTick(void* pFunc_, CGLFrm* _pFrame)
	{
		if(!_pFrame->onFrame())
		{
			pFunc_ = &MainLoop_DoNothing;
		}
	}
	void MainLoop_TestMainFrame(void* pFunc_, CGLFrm* _pFrame)
	{
		if (_pFrame)
		{
			pFunc_ = _pFrame->onFrame() ? &MainLoop_RealTick : &MainLoop_DoNothing;
		}
	}
}
void CGLApp::onReleaseMainFrame()
{
	m_pMainLoopFunc = &MainLoop_DoNothing;
}

int CGLApp::Run()
{
	MSG msg;
	m_pMainLoopFunc = &MainLoop_TestMainFrame;
	// acquire and dispatch messages until a WM_QUIT message is received.
	while(1)
	{
		if(::PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
			{
				break;
			}
			::DispatchMessage(&msg);
			::TranslateMessage(&msg);
		}

		m_pMainLoopFunc(m_pMainLoopFunc, (CGLFrm*)m_pMainWnd);
	}
	ExitInstance();
	return 0;
}

engine::IAppHandler* CGLApp::getAppHandler()
{
	return m_pAppHandler;
}

