#ifndef LIB_SIMCITY_TERRAIN_PATCH_H
#define LIB_SIMCITY_TERRAIN_PATCH_H


#include "math_inc.h"
namespace libSimCity{

	class TerrainPatch
	{
	public:
		TerrainPatch();
		~TerrainPatch();
		void AddToRender();
		void setPos(float _x, float _z);

		float*			getHeightMap();
		const float*	getHeightMap() const;
		math::vec2		getPosition() const;
	private:
		math::vec2	m_vPostion;
		float*		m_pHeightMap;
	};

	class TerrainUnit
	{
	public:
		TerrainUnit(int _xid, int _zid);
		void AddToRender();
	private:
		static const int UNIT_PATCH_SIZE = 4;
		static const int UNIT_PATCH_CNT = UNIT_PATCH_SIZE * UNIT_PATCH_SIZE;
		TerrainPatch m_vPatch[UNIT_PATCH_CNT];
		int m_ID[2];
	};

	class Terrain
	{
	public:
		Terrain();
	private:
		TerrainUnit* m_pUnits;
	};

}
#endif 