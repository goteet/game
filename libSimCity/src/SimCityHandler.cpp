#include "SimCityHandler.h"
#include "handler.h"
#include "engine/context.h"
#include "math_inc.h"
#include "RenderGrid.h"
#include "Camera.h"
#include "file.h"

#include <windows.h>

#include "render_char.h"

#include "game.h"

using namespace libSimCity;

class SimCityHandler : public engine::IAppHandler
{
public:
	virtual bool onFrame();
	virtual void onRender();

	virtual bool onInitial();
	virtual void onDestroy();

	virtual void* onCommand(void*);
	virtual void onResize(int _nWidth, int _nHeight);

	virtual void onDragBeginL(int _x, int _y, int _ctrl, int _shift, int _alt);
	virtual void onDragBeginR(int _x, int _y, int _ctrl, int _shift, int _alt)  {}
	virtual void onDragBeginM(int _x, int _y, int _ctrl, int _shift, int _alt)  {}
    virtual void onDragEndL(int _x, int _y, int _ctrl, int _shift, int _alt)    {}
	virtual void onDragEndR(int _x, int _y, int _ctrl, int _shift, int _alt)    {}
	virtual void onDragEndM(int _x, int _y, int _ctrl, int _shift, int _alt)    {}
    virtual void onDraggingL(int _x, int _y, int _ctrl, int _shift, int _alt)	{}
	virtual void onDraggingR(int _x, int _y, int _ctrl, int _shift, int _alt)	{}
	virtual void onDraggingM(int _x, int _y, int _ctrl, int _shift, int _alt)	{}

	virtual void onKeyDown(int _key, int _ctrl, int _shift, int _alt);
private:
	graphic::Device* getDevice();

	math::mat4 m_mat_vp_grid;
	math::mat4 m_mat_vp_game;

	RenderChar m_renderchar;


private:
	game::Game* m_p_game;
};

engine::IAppHandler* CreateSimCityHandler()
{
	return new SimCityHandler();
}

bool SimCityHandler::onFrame()
{

	if(m_p_game->is_finished()!=game::Game::RUN)
	{
		m_p_game->uninit();
		m_p_game->init();
	}
	else
	{
		m_p_game->update();
	}
	

	return true;
}
#include <fstream>
bool SimCityHandler::onInitial()
{
	const int iphone_width	= 640;
	const int iphone_height	= 960;

	m_p_game = new game::Game(iphone_width,iphone_height);
	m_p_game->init();

	if(!m_renderchar.Initial(getDevice()))
	{
		return false;
	}
	

	RenderGrid::instance()->Init(getDevice());


	math::vec3 eye	= math::vec3(0,10,0);
	math::vec3 at	= math::vec3(0,0,0);
	math::vec3 up	= math::vec3(0,0,1);
	math::mat4 m_mat_v_grid = math::lookat_rh(eye, at, up);

	eye	= math::vec3(0,0,10);
	at	= math::vec3(0,0,0);
	up	= math::vec3(0,1,0);


	math::mat4 mat_v_game = math::lookat_rh(eye, at, up);
	math::mat4 mat_p = math::ortho_rh(iphone_width,iphone_height,0,500);

	m_mat_vp_grid	= m_mat_v_grid * mat_p;
	m_mat_vp_game	= mat_v_game * mat_p;

	
	return true;
}

void SimCityHandler::onDestroy()
{
	m_renderchar.uninit();
	RenderGrid::instance()->Deinit();
	m_p_game->uninit();
	delete m_p_game;
}

void* SimCityHandler::onCommand( void* )
{
	return 0;
}

graphic::Device* SimCityHandler::getDevice()
{
	return engine::AppContext::instance()->pDevice;
}

void SimCityHandler::onRender()
{

	RenderGrid::instance()->Render(m_mat_vp_grid.mp);


	int enemy_cnt = m_p_game->m_enemy_list.size();
	if(enemy_cnt)
	{
		math::vec3 color(1.0f);
		m_renderchar.render(
			m_mat_vp_game.mp, 
			(game::Character**)&(m_p_game->m_enemy_list[0]),
			enemy_cnt, color.v);

		color.set(1.0f, 0.0f, 0.0f);
		m_renderchar.render(
			m_mat_vp_game.mp, 
			(game::Character**)&(m_p_game->m_hero),
			1, color.v);
	}
	

	
}

void SimCityHandler::onResize( int _nWidth, int _nHeight )
{
}

void SimCityHandler::onDragBeginL(int _x, int _y, int _ctrl, int _shift, int _alt)
{
}
#include "character.h"
void SimCityHandler::onKeyDown( int _key, int _ctrl, int _shift, int _alt )
{
	switch(_key)
	{
	case ARROW_L:
		m_p_game->m_hero->m_body->ApplyForceToCenter(math::vec2(-0.1f,0.0f));
		break;
	case ARROW_R:
		m_p_game->m_hero->m_body->ApplyForceToCenter(math::vec2(0.1f,0.0f));
		break;
	case ARROW_U:
		m_p_game->m_hero->m_body->ApplyForceToCenter(math::vec2(0.0f, 0.1f));
		break;
	case ARROW_D:
		m_p_game->m_hero->m_body->ApplyForceToCenter(math::vec2(0.0f,-0.1f));
		break;
	default:
		break;
	}
	
}
