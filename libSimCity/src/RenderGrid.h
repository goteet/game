#pragma once
#include <vector>
#include "device.h"
#include "EffectGrid.h"

class RenderGrid
{
public:
	static RenderGrid* instance();
	bool Init(graphic::Device* device);
	void Deinit();
	void Render(float* matVP);
private:
	enum{ GRID_SIZE = 32+1};

	graphic::Device*		m_pDevice;
	graphic::VertexBuffer*	m_pVB;
	EffectGrid* m_pEffect;

	void InitVB();
	void InitDecl();
};
