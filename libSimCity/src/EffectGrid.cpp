#include "EffectGrid.h"


void EffectGrid::OnDestroy()
{
	if(m_pMatVP)	m_pMatVP->Release();
}

void EffectGrid::GetParams()
{
	m_pMatVP	= GetParam("u_MatVP");
}

const char* EffectGrid::GetVertexShader()
{
	static const char* s_szVertexShader = {
		"#version 110\n"
		"uniform mat4 u_MatVP;"  
		"attribute vec3 in_xz_grayscale;"
		"varying float fGrayScale;"
		"void main(){"
		"	gl_Position	= u_MatVP * "
		"		vec4(in_xz_grayscale.x, 0.0, in_xz_grayscale.y, 1.0);"
		"	fGrayScale = in_xz_grayscale.z;"
		"}\0\0"
	};
	return s_szVertexShader;
}

const char* EffectGrid::GetPixelShader()
{
	static const char* s_szFragShader = {
		"#version 110\n"
		"varying float fGrayScale;"
		"void main(){"
		"   gl_FragColor = vec4(fGrayScale, fGrayScale, fGrayScale, 1.0);"
		"}\0\0"  
	};
	return s_szFragShader;
}

graphic::AttribLoc* EffectGrid::GetAttribLoc()
{
	static graphic::AttribLoc loc[] =
	{
		{0,	"in_xz_grayscale"},
	};
	return loc;
}

int EffectGrid::GetAttribLocCount()
{
	return 1;
}

void EffectGrid::SetMatrixVP( float* _pMatrixVP )
{
	if(m_pMatVP)m_pMatVP->SetMatrix(_pMatrixVP);
}

