#include "render_char.h"


libSimCity::EffectChar::EffectChar()
: m_p_mat_vp(0)
, m_p_color(0)
{

}

void libSimCity::EffectChar::GetParams()
{
	m_p_mat_vp	= GetParam("u_mat_vp");
	m_p_color	= GetParam("u_v3_color");
}

const char* libSimCity::EffectChar::GetVertexShader()
{
	static const char* s_sz_vertex_shader = {
		"uniform mat4 u_mat_vp;"
		"attribute vec2 in_coord;"
		"attribute vec2 in_uv;"
		"attribute vec2 in_pos;"
		"void main(){"
		"	vec4 w_pos = vec4(in_pos,  0.0, 1.0);"
		"	vec4 m_pos = vec4(in_coord,0.0, 0.0);"
		"	gl_Position	= u_mat_vp * vec4(w_pos+m_pos*10);"
		"}\0\0"
	};
	return s_sz_vertex_shader;
}

const char* libSimCity::EffectChar::GetPixelShader()
{
	static const char* s_sz_frag_shader = {
		"uniform vec3 u_v3_color;"
		"void main(){"
		"   gl_FragColor = vec4(u_v3_color, 1.0);"
		"}\0\0"
	};
	return s_sz_frag_shader;
}

graphic::AttribLoc* libSimCity::EffectChar::GetAttribLoc()
{
	static graphic::AttribLoc loc[] =
	{
		{0,	"in_coord"},
		{1,	"in_uv"},
		{2,	"in_pos"},
	};
	return loc;
}

int libSimCity::EffectChar::GetAttribLocCount()
{
	return 3;
}

void libSimCity::EffectChar::OnDestroy()
{
	if(m_p_mat_vp)	m_p_mat_vp->Release();
	if(m_p_color)	m_p_color->Release();
}

void libSimCity::EffectChar::SetMatrixVP(float* _p_mat_vp)
{
	if(m_p_mat_vp)m_p_mat_vp->SetMatrix(_p_mat_vp);
}

void libSimCity::EffectChar::SetColor(float* _v3_color)
{
	if(m_p_color)m_p_color->SetFloat3(_v3_color);
}
