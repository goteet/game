#include "render_char.h"

#include "device.h"
#include "math_inc.h"

//offsetof
#include "stddef.h"
#include "character.h"
namespace {
#pragma pack(1)
	struct CharGrid
	{
		math::vec2 coord;
		math::vec2 uv;
	};
	struct CharPos
	{
		math::vec2 pos;
	};
}
libSimCity::RenderChar::RenderChar()
: m_pDevice(0)
, m_p_vb_pos(0)
, m_p_vb_grid(0)
, m_p_ib(0)
, m_pShader(0)
, m_pTexManager(0)
{
	
}

bool libSimCity::RenderChar::Initial(graphic::Device* _pDevice)
{
	if(_pDevice)
		m_pDevice = _pDevice;

	m_pShader = new EffectChar();
	m_pShader->Init(m_pDevice);
	
	
	m_pTexManager = engine::CreateTexManager(_pDevice, io::GetDefFileSysC());
	
	
	if(!init_vb_pos()
	|| !init_vb_grid()
	|| !init_ib()
	|| !init_decl()
	)
	{
		uninit();
		return false;
	}
	
	return true;
}

void libSimCity::RenderChar::uninit()
{
	uninit_vb();
	uninit_ib();
	if(m_pTexManager)
		m_pTexManager->Release();
	if(m_pShader)
		m_pShader->Release();
		
}

void libSimCity::RenderChar::render(float* _p_mat_vp, game::Character** _char_list, int _char_cnt, float* _v3_color)
{
	
	m_pShader->Begin();
	//m_pDevice->SetRenderStage(graphic::RS_WIREFRAME, true);
	m_pDevice->SetRenderStage(graphic::RS_DEPETH_TEST, true);

	
	m_pShader->SetMatrixVP(_p_mat_vp);
	m_pShader->SetColor(_v3_color);
	
	/*
	graphic::Texture* p_texture = m_pTexManager->LoadTexture(_szMaterial);
	if(p_texture)
	{
		p_texture->SetWrapS(graphic::Texture::REPEAT);
		p_texture->SetWrapT(graphic::Texture::REPEAT);
		p_texture->SetFilterMag(graphic::Texture::LINEAR);
		p_texture->SetFilterMin(graphic::Texture::LINEAR);
		m_pShader->SetTexture(p_texture);
	}

	m_p_vb_pos->Upload((const graphic::byte*)_pHeight, VTX_CNT * sizeof(float));
	*/
	update_char_pos(_char_list, _char_cnt);
	

	m_pDevice->SetVertexBuffer(0, m_p_vb_grid);
	m_pDevice->SetVertexBuffer(1, m_p_vb_pos);
	m_pDevice->SetIndexBuffer(m_p_ib);

	m_pDevice->DrawIndexed(graphic::PT_TRIANGLES, 0, _char_cnt * 6);


	m_pDevice->SetRenderStage(graphic::RS_DEPETH_TEST, false);
	//m_pDevice->SetRenderStage(graphic::RS_WIREFRAME, false);
	m_pShader->End();
	
}

#define IDX(x,i) ((x)+(i) * VTX_SIZE)
#include <stdlib.h>
#include <ctime>
bool libSimCity::RenderChar::init_vb_pos()
{
	m_p_vb_pos = m_pDevice->CreateVertexBuffer(
		sizeof(CharPos), MAX_CHAR_VTX,
		graphic::IBuffer::DYNAMIC_DRAW);
	if(!m_p_vb_pos)
	{
		return false;
	}

	CharPos* p_vb_ptr = (CharPos*)m_p_vb_pos->Lock(graphic::IBuffer::WRITE_ONLY);
	if(!p_vb_ptr)
	{
		return false;
	}

	for(int i = 0; i != MAX_CHAR_VTX; i++)
	{
		p_vb_ptr[i].pos = math::vec2(0.0f, 0.0f);
	}

	m_p_vb_pos->Unlock();

	return true;
}

bool libSimCity::RenderChar::init_vb_grid()
{
	m_p_vb_grid = m_pDevice->CreateVertexBuffer(
		sizeof(CharGrid), MAX_CHAR_VTX,
		graphic::IBuffer::STATIC_DRAW);

	if(!m_p_vb_grid)
	{
		return false;
	}

	CharGrid* p_vb_ptr = (CharGrid*)m_p_vb_grid->Lock(graphic::IBuffer::WRITE_ONLY);
	if(!p_vb_ptr)
	{
		return false;
	}

	for(int i = 0; i != MAX_CHAR_CNT; i++)
	{
		int vtx_offset = i*4;
		CharGrid* pVertexFix = p_vb_ptr + vtx_offset;

		/*	0 1
			2 3	*/
		pVertexFix[0].coord = math::vec2(-0.5f,  0.5f);
		pVertexFix[1].coord = math::vec2( 0.5f,  0.5f);
		pVertexFix[2].coord = math::vec2(-0.5f, -0.5f);
		pVertexFix[3].coord = math::vec2( 0.5f, -0.5f);

		pVertexFix[0].uv = math::vec2(0.0f, 0.0f);
		pVertexFix[1].uv = math::vec2(1.0f, 0.0f);
		pVertexFix[2].uv = math::vec2(0.0f, 1.0f);
		pVertexFix[3].uv = math::vec2(1.0f, 1.0f);
	}
	m_p_vb_grid->Unlock();
	
	return true;
}

bool libSimCity::RenderChar::init_ib()
{
	m_p_ib = m_pDevice->CreateIndexBuffer(MAX_IDX_CNT, graphic::IBuffer::STATIC_DRAW);
	if(!m_p_ib)
	{
		return false;
	}
	graphic::ushort* p_ib_ptr = (graphic::ushort*)m_p_ib->Lock(graphic::IBuffer::WRITE_ONLY);
	if(!p_ib_ptr)
	{
		return false;
	}

	for(int i = 0; i != MAX_CHAR_CNT; i++)
	{
		int idx_offset = i * 6;
		int vtx_offset = i * 4;
		graphic::ushort* p_index = p_ib_ptr+idx_offset;

		/*	0 1
			2 3	*/
		p_index[0] = 0+vtx_offset;
		p_index[1] = 2+vtx_offset;
		p_index[2] = 1+vtx_offset;

		p_index[3] = 1+vtx_offset;
		p_index[4] = 2+vtx_offset;
		p_index[5] = 3+vtx_offset;
	}
	m_p_ib->Unlock();

	return true;
}
#undef IDX

bool libSimCity::RenderChar::init_decl()
{

	const int DECL_SIZE = 3;
	graphic::AttribDecl decl_arrary[DECL_SIZE];

	int idx = 0;
	decl_arrary[idx].sematic_loc	= 0;
	decl_arrary[idx].inner_offset	= offsetof(CharGrid, coord);
	decl_arrary[idx].vec_size		= graphic::AttribDecl::DECL_FLOAT2;


	idx = 1;
	decl_arrary[idx].sematic_loc	= 1;	
	decl_arrary[idx].inner_offset	= offsetof(CharGrid, uv);
	decl_arrary[idx].vec_size		= graphic::AttribDecl::DECL_FLOAT2;
	
	idx = 2;
	decl_arrary[idx].sematic_loc	= 2;	
	decl_arrary[idx].inner_offset	= 0;
	decl_arrary[idx].vec_size		= graphic::AttribDecl::DECL_FLOAT2;

	m_p_vb_grid->SetVertexDecl(decl_arrary, 2);
	m_p_vb_pos->SetVertexDecl(decl_arrary+2, 1);
	return true;
}

void libSimCity::RenderChar::uninit_vb()
{
	if(m_p_vb_grid)	m_p_vb_grid->Release();
	if(m_p_vb_pos)	m_p_vb_pos->Release();
}

void libSimCity::RenderChar::uninit_ib()
{
	if(m_p_ib)	m_p_ib->Release();
}

void libSimCity::RenderChar::update_char_pos(game::Character** _char_list, int _char_cnt )
{
	CharPos* p_pos = (CharPos*)m_p_vb_pos->Lock(graphic::IBuffer::WRITE_ONLY);
	math::vec2 pos;
	for(int i = 0; i < _char_cnt; i++)
	{
		game::Character* p_char = _char_list[i];
		math::vec2 b2_pos = p_char->m_body->GetPosition();
		pos = math::vec2(b2_pos.x, b2_pos.y);
		p_pos[0].pos = pos;
		p_pos[1].pos = pos;
		p_pos[2].pos = pos;
		p_pos[3].pos = pos;
		p_pos+=4;
	}
	m_p_vb_pos->Unlock();
}
