#pragma once

#include "shader.h"
class EffectGrid : public graphic::IEffect
{
public:
	virtual void OnDestroy();
	virtual void GetParams();
	virtual const char* GetVertexShader();
	virtual const char* GetPixelShader();
	virtual graphic::AttribLoc* GetAttribLoc();
	virtual int	GetAttribLocCount();

	void SetMatrixVP(float* _pMatrixVP);

	graphic::ShaderParam* m_pMatVP;
};