#ifndef LIB_SIMCITY_RENDER_CHARACTER_H
#define LIB_SIMCITY_RENDER_CHARACTER_H

#include "device.h"
#include "render/tex_mgr.h"
namespace game{
	struct Character;
}
namespace libSimCity{

	class EffectChar : public graphic::IEffect
	{
	public:
		EffectChar();
		virtual ~EffectChar(){}
		virtual const char* GetVertexShader();
		virtual const char* GetPixelShader();
		virtual graphic::AttribLoc* GetAttribLoc();
		virtual int	GetAttribLocCount();
		virtual void OnDestroy();
		virtual void GetParams();

		void SetColor(float* _v3_color);
		void SetMatrixVP(float* _pMatVP);
		void SetTexture(graphic::Texture* _pTex);

	private:
		graphic::ShaderParam* m_p_color;
		graphic::ShaderParam* m_p_mat_vp;
		graphic::ShaderParam* m_pTexture;
	};

	class RenderChar{
	public:
		RenderChar();
		bool Initial(graphic::Device* _pDevice);
		void uninit();
		void render(float* _pMatVP, game::Character** _char_list, int _char_cnt, float* _v3_color);

	private:
		graphic::Device* m_pDevice;

	private:
		bool init_vb_grid();
		bool init_vb_pos();
		bool init_ib();
		bool init_decl();
		void uninit_vb();
		void uninit_ib();
		void update_char_pos(game::Character** _char_list, int _char_cnt );
		graphic::VertexBuffer*	m_p_vb_pos;
		graphic::VertexBuffer*	m_p_vb_grid;
		graphic::IndexBuffer*	m_p_ib;

		engine::TexManager*		m_pTexManager;

		EffectChar* m_pShader;

	private:

		static const int MAX_CHAR_CNT	= 256;
		static const int MAX_CHAR_VTX	= MAX_CHAR_CNT*4;
		static const int MAX_IDX_CNT	= MAX_CHAR_CNT * 6;
	};


}
#endif 