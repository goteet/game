#include "RenderGrid.h"

#include "device.h"
#include "math_inc.h"


RenderGrid* RenderGrid::instance()
{
	static RenderGrid inst;
	return &inst;
}




bool RenderGrid::Init(graphic::Device* _pDevice )
{
	if(!_pDevice)
		return false;

	m_pDevice = _pDevice;

	InitVB();
	InitDecl();
	m_pEffect = new EffectGrid();
	m_pEffect->Init(_pDevice);

	return true;
}


void RenderGrid::Deinit()
{
	m_pEffect->Release();
	m_pEffect = 0;
	m_pVB->Release();
}



void RenderGrid::Render(float* _pMatrixVP)
{

	m_pEffect->Begin();
	m_pDevice->SetRenderStage(graphic::RS_DEPETH_TEST, false);
	//glBindTexture(GL_TEXTURE_2D, 0);
	m_pEffect->SetMatrixVP(_pMatrixVP);

	m_pDevice->SetVertexBuffer(0, m_pVB);

	m_pDevice->Draw(graphic::PT_LINES, 0, GRID_SIZE * 4);
	m_pDevice->SetRenderStage(graphic::RS_DEPETH_TEST, true);
	m_pEffect->End();

}


namespace {
	struct VertexGrid{
		math::vec3 xz_grayscale;
	};
}
void RenderGrid::InitVB()
{
	const float iphone_width	= 640;
	const float iphone_height	= 960;

	const int VB_SIZE	= GRID_SIZE * 4;
	m_pVB = m_pDevice->CreateVertexBuffer(sizeof(VertexGrid), VB_SIZE, graphic::IBuffer::STATIC_DRAW);

	VertexGrid* pVB = (VertexGrid*)m_pVB->Lock(graphic::IBuffer::WRITE_ONLY);

	//ˮƽ��
	float GRID_STEP	= iphone_height / (GRID_SIZE-1);
	float GRID_LEN	= iphone_width;

	const float GRAY_INT	= 0.4f;

	for(int i = 0; i != GRID_SIZE; i++)
	{
		float x = GRID_LEN;
		float z = GRID_STEP * (i - GRID_SIZE/2);

		pVB->xz_grayscale = math::vec3(-x,z,GRAY_INT);		pVB++;
		pVB->xz_grayscale = math::vec3(x,z,GRAY_INT);		pVB++;
	}

	GRID_STEP	= iphone_width / (GRID_SIZE-1);
	GRID_LEN	= iphone_height;

	for(int i = 0; i != GRID_SIZE; i++)
	{
		float z = GRID_LEN;
		float x = GRID_STEP * (i - GRID_SIZE/2);
		pVB->xz_grayscale = math::vec3(x,-z,GRAY_INT);		pVB++;
		pVB->xz_grayscale = math::vec3(x,z,GRAY_INT);		pVB++;
	}
	m_pVB->Unlock();
}

void RenderGrid::InitDecl()
{
	const int DECL_SIZE = 1;
	graphic::AttribDecl declArray[DECL_SIZE];
	declArray[0].sematic_loc	= 0;
	declArray[0].inner_offset	= 0;
	declArray[0].vec_size		= graphic::AttribDecl::DECL_FLOAT3;

	m_pVB->SetVertexDecl(declArray, DECL_SIZE);
}