#include "Camera.h"
#include <cmath>
Camera::Camera() : MAX_HEIGHT(500.0f)
	, MIN_HEIGHT(10.0f)
	, MAX_PITCH(89.0f)
	, MIN_PITCH(15.0f)
{
	vEyePt		= math::vec3( 0.0f, 10.0f,  10.0f);
	vUpVec		= math::vec3( 0.0f, 1.0f,	0.0f );
	vForwardVec = math::vec3( 0.0f, 0.0f,	-1.0f );

	aYaw	= 0.0f;
	aPitch	= MIN_PITCH;
	SetYaw(180.0f);
	SetPitch(MIN_PITCH);
	bNeedUpdateView = true;
	bNeedUpdateProj = true;

	BuildViewMatrix();
	BuidProjMatrx(800,600);
}

void Camera::SetHeight( float h )
{
	vEyePt.y += h;
	if(vEyePt.y > MAX_HEIGHT) vEyePt.y = MAX_HEIGHT;
	else if(vEyePt.y < MIN_HEIGHT) vEyePt.y = MIN_HEIGHT;

	float rPitch	= (90.0f - aPitch) * math::PI_A2R;
	nDist = vEyePt.y * tan(rPitch);

	bNeedUpdateView = true;
}

void Camera::MoveLeft( float d )
{
	vEyePt += vLeftVec * d;
	bNeedUpdateView = true;
}

void Camera::MoveRight( float d )
{
	vEyePt -= vLeftVec * d;
	bNeedUpdateView = true;
}

void Camera::MoveForward( float d )
{
	vEyePt += vForwardVec * d;
	bNeedUpdateView = true;
}

void Camera::MoveBackward( float d )
{
	vEyePt -= vForwardVec * d;
	bNeedUpdateView = true;
}

void Camera::SetYaw( float yaw )
{
	aYaw = yaw;
	while(aYaw > 360)	aYaw -= 360;
	while(aYaw < 0)		aYaw += 360;
	float rYaw		= aYaw * math::PI_A2R;
	vForwardVec.x = sin(rYaw);
	vForwardVec.z = cos(rYaw);
	vLeftVec = cross(vForwardVec, vUpVec).normalize();
	bNeedUpdateView = true;
}

void Camera::SetPitch( float pitch )
{
	if(pitch > MAX_PITCH)		aPitch = MAX_PITCH;
	else if(pitch < MIN_PITCH)	aPitch = MIN_PITCH;
	else						aPitch = pitch;
	pitch = 90.0f - aPitch;
	float rPitch	= pitch * math::PI_A2R;
	nDist = vEyePt.y * tan(rPitch);

	bNeedUpdateView = true;
}

void Camera::BuildViewMatrix()
{
	/*
	if(!bNeedUpdateView)
		return;
*/
	vLookatPt = vEyePt + vForwardVec * nDist;
	vLookatPt.y = 0;

	bNeedUpdateView = false;
	matView = math::lookat_rh(vEyePt, vLookatPt, vUpVec);
}

void Camera::BuidProjMatrx(int width, int height)
{
	/*
	if(!bNeedUpdateProj)
		return;
	*/
	
	bNeedUpdateProj = false;
	const float aspect = width * 1.0f / height;
	matProj = math::proj_fov_rh(math::PI_4, aspect, 1.0f, 500.0f);
}

math::mat4* Camera::GetViewMatrix()
{
	return &matView;
}

math::mat4* Camera::GetProjMatrix()
{
	return &matProj;
}

void Camera::GetLookAt( float*x, float*z )
{
	if(x) *x= vLookatPt.x;
	if(z) *z= vLookatPt.z;
}

math::mat4* Camera::GetWorldMatrix()
{
	static math::mat4 mWorld;
	mWorld = math::translate(vEyePt);
	return &mWorld;
}
///////////////////////////////////////////////////////////////////////////////
