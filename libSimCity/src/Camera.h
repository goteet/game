#pragma once

#include "math_inc.h"
class Camera
{
public:
	float aPitch;
	float aYaw;
	float nDist;

	const float MAX_HEIGHT;
	const float MIN_HEIGHT;
	const float MAX_PITCH;
	const float MIN_PITCH;
	Camera();
	void SetHeight(float h);
	void MoveLeft(float d);
	void MoveRight(float d);
	void MoveForward(float d);
	void MoveBackward(float d);

	void SetYaw(float yaw);

	void SetPitch(float pitch);

	void BuildViewMatrix();
	void BuidProjMatrx(int width, int height);
	math::mat4* GetViewMatrix();
	math::mat4* GetProjMatrix();
	math::mat4* GetWorldMatrix();
	void GetLookAt(float*x, float*z);

//private:
	math::mat4	matView;
	math::mat4	matProj;

	math::vec3 vEyePt;  
	math::vec3 vLookatPt;
	math::vec3 vUpVec;
	math::vec3 vForwardVec;
	math::vec3 vLeftVec;
	bool bNeedUpdateView;
	bool bNeedUpdateProj;
};