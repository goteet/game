#include "../include/game.h"
#include "../include/timer.h"
#include "../include/character.h"
#include <assert.h>
#include <ctime>
#include "scene.h"

#include "Box2D/Box2D.h"

namespace{
	float get_random_float()
	{
		return 2.0f * ((rand() % RAND_MAX)/ (RAND_MAX-1.0f) - 0.5f);
	}
	float get_random_range(float _half_range)
	{
		return _half_range * get_random_float();
	}
	math::vec2 get_random_pos(math::vec2 _center)
	{
		const float half_range = 20;
		float x,y;
		do{
			x = get_random_range((float)game::g_scene.m_width / 2);
			y = get_random_range((float)game::g_scene.m_height / 2);
		}
		while(x < _center.x + half_range
		   && x > _center.x - half_range
		   && y < _center.y + half_range
		   && y > _center.y - half_range);

		return math::vec2(x, y);
	}
}
void game::Game::init()
{
	srand((unsigned int)time(0));
	initial_physical_world();
	initial_player_hero();
	initial_enemy_timer();
	clear_enemy_list();
	
}

void game::Game::uninit()
{
	destroy_physical_world();
	destroy_enemy_timer();
	destroy_player_hero();
	
	clear_enemy_list();
}

void game::Game::update()
{
	if(!is_generated_all_enemy())
	{
		this->m_timer_enemy->tick();
		if(this->m_timer_enemy->expired())
		{
			this->m_timer_enemy->reset();
			generate_enemies();
		}
	}
	
	//pos of hero will be update by
	//user input interface.
	//not concerned yet.

	
	update_enemies_state();
	float32 time_step = 1000.0f / 60.0f;
	m_physical_world.update(time_step);
}

game::Game::e_finish_state game::Game::is_finished()
{
	bool hitted = hittest_enemies_and_hero();
	if(hitted)
	{
		return LOSE;
	}
	else if(is_generated_all_enemy() && get_enemy_count() == 0)
	{
		return WIN;
	}
	else
	{
		return RUN;
	}

}

void game::Game::initial_enemy_timer()
{
	const int timer_interval = 10 * Timer::MSEC;
	m_timer_enemy	= new Timer(timer_interval);
	m_enemy_wave	= 15;
}

void game::Game::destroy_enemy_timer()
{
	if(m_timer_enemy)
	{
		delete m_timer_enemy;
		m_timer_enemy = 0;
	}
	m_enemy_wave = 0;
}

game::Game::Game(int _scene_width, int _scene_height)
: m_timer_enemy(0)
, m_enemy_wave(0)
{
	
	g_scene.m_width = _scene_width;
	g_scene.m_height = _scene_height;
}

bool game::Game::is_generated_all_enemy()
{
	if(m_enemy_wave == 0)
		return true;
	return false;
}

void game::Game::generate_enemies()
{
	
	//generate
	//生成是核心。全屏生成敌人
	//希望能在玩家周围一个范围内不生成
	//随着难度提升，范围缩小。
	math::vec2 hero_pos = m_hero->get_position();

	const int ENEMY_CNT_EACH_WAVE = 10;
	int enemy_cnt = m_enemy_list.size();
	int enemy_cnt_new = enemy_cnt + ENEMY_CNT_EACH_WAVE;

	m_enemy_list.resize(enemy_cnt_new);
	for(int i = enemy_cnt; i < enemy_cnt_new; i++)
	{
		Enemy*& enemey = m_enemy_list[i];

		math::vec2 enemy_pos = get_random_pos(hero_pos);
		math::vec2 enemy_vel = math::vec2(get_random_float(), get_random_float());
		enemy_vel.normalize();
		
		enemey = new Enemy();		
		enemey->m_body = m_physical_world.create_enemy_physic_body(enemy_pos, enemy_vel);
	}
	m_enemy_wave--;
}

void game::Game::initial_player_hero()
{
	m_hero = new Character();
	m_hero->m_body = m_physical_world.create_hero_physic_body();

}

void game::Game::destroy_player_hero()
{
	if(m_hero)
	{
		delete m_hero;
		m_hero = 0;
	}
}

int game::Game::get_enemy_count()
{
	return m_enemy_list.size();
}

void game::Game::clear_enemy_list()
{
	if(get_enemy_count() == 0)
		return;
	EnemyIter visit_iter = m_enemy_list.begin();
	EnemyIter char_list_end = m_enemy_list.end();
	while(visit_iter != char_list_end)
	{
		Enemy* p_enemy = *visit_iter;
		
		delete p_enemy;
		p_enemy = 0;

		visit_iter++;
	}
	m_enemy_list.clear();
}

void game::Game::update_enemies_state()
{
	int enemy_cnt = get_enemy_count();
	if(enemy_cnt == 0)
		return;

	for(int i = 0; i < enemy_cnt; i++)
	{
		Enemy* p_enemy = m_enemy_list[i];

		if(p_enemy->is_dead())
		{
			if(i != enemy_cnt -1)
			{
				m_enemy_list[i] = m_enemy_list[enemy_cnt -1];
			}
			m_physical_world.release(p_enemy->m_body);
			delete p_enemy;
			m_enemy_list.pop_back();
			enemy_cnt--;
		}
	}
}

bool game::Game::hittest_enemies_and_hero()
{
	if(get_enemy_count() == 0)
			return false;

	EnemyIter visit_iter = m_enemy_list.begin();
	EnemyIter char_list_end = m_enemy_list.end();
	while(visit_iter != char_list_end)
	{
		Enemy*& p_enemy = *visit_iter;
		

		/*
		if(hero_bound->hit_test(enemy_bound))
			return true;
		*/

		visit_iter++;
	}
	return false;
}

void game::Game::initial_physical_world()
{
	m_physical_world.init();
}

void game::Game::destroy_physical_world()
{
	m_physical_world.uninit();
}