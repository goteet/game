#include "../include/character.h"
#include "scene.h"
game::Character::Character()
{
	
}

game::Character::~Character()
{
}

math::vec2 game::Character::get_position()
{
	return m_body->GetPosition();
}

bool game::Enemy::is_dead()
{
	math::vec2 position = get_position();
	if(position.x < -game::g_scene.m_width
	|| position.x >  game::g_scene.m_width
	|| position.y < -game::g_scene.m_height
	|| position.y >  game::g_scene.m_height)
		return true;
	return false;
}
