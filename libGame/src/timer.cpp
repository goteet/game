#include "../include/timer.h"

const int game::Timer::MSEC = 1;
const int game::Timer::SEC = 1000;

game::Timer::Timer( int _interval )
: m_elapse_stamp(0)
, m_expired_cnt(_interval)
{

}

void game::Timer::tick()
{
	m_elapse_stamp++;
}

bool game::Timer::expired()
{
	//return true;
	if(m_expired_cnt <= m_elapse_stamp)
		return true;
	else
		return false;
}

void game::Timer::reset()
{
	m_elapse_stamp = 0;
}
