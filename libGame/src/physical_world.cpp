#include "../include/physical_world.h"
#include "math_inc.h"
#include "Box2D/Box2D.h"
void game::PhysicalWorld::init()
{
	math::vec2 gravity = math::vec2(0, 0);
	m_p_world = new b2World(gravity);
}

void game::PhysicalWorld::uninit()
{
	if(m_p_world)
	{
		delete m_p_world;
		m_p_world = 0;
	}
}

void game::PhysicalWorld::update(float _dt)
{
	int32 vel_iter = 8;
	int32 pos_iter = 8;
	m_p_world->Step(_dt, vel_iter, pos_iter);
}

b2Body* game::PhysicalWorld::create_enemy_physic_body(const math::vec2& _pos, const math::vec2& _velocity)
{
	b2BodyDef body_def;
	body_def.type = b2_dynamicBody;
	body_def.position = _pos;
	body_def.linearVelocity = _velocity;

	b2PolygonShape shape_box;
	shape_box.SetAsBox(5.0f, 5.0f);

	b2FixtureDef fixture_define;
	fixture_define.shape = &shape_box;
	fixture_define.density = 1.0f;
	fixture_define.friction = 0.0f;

	//临时关闭测试
	fixture_define.isSensor = false;

	b2Body* body = m_p_world->CreateBody(&body_def);
	body->CreateFixture(&fixture_define);
	return body;
}

b2Body* game::PhysicalWorld::create_hero_physic_body()
{
	b2BodyDef body_def;
	body_def.type = b2_dynamicBody;
	//设置中心位置
	body_def.position.set(0.0f);
	body_def.linearVelocity.set(0.0f);

	b2PolygonShape shape_box;
	shape_box.SetAsBox(5.0f, 5.0f);

	b2FixtureDef fixture_define;
	fixture_define.shape = &shape_box;
	fixture_define.density = 1.0f;
	fixture_define.friction = 0.0f;

	b2Body* body = m_p_world->CreateBody(&body_def);
	body->CreateFixture(&fixture_define);
	return body;
}


void game::PhysicalWorld::release( b2Body* _body)
{

}
