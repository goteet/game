#ifndef LIB_GAME_SCENE_H
#define LIB_GAME_SCENE_H

namespace game
{
	struct Scene
	{
		int m_width;
		int m_height;
	};

	extern Scene g_scene;
}
#endif 