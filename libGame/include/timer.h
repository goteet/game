#ifndef LIB_GAME_TIMER_H
#define LIB_GAME_TIMER_H
namespace game
{
	class Timer
	{
	public:
		static const int MSEC;
		static const int SEC;

		Timer(int _interval);

		void tick();
		void reset();
		bool expired();
		
	private:
		int m_elapse_stamp;
		int m_expired_cnt;
	};
}
#endif 