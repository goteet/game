#ifndef LIB_GAME_PHYSICAL_WORLD_H
#define LIB_GAME_PHYSICAL_WORLD_H

#include "Box2D/Box2D.h"
#include "math_inc.h"
namespace game
{
	class PhysicalWorld
	{
	public:
		void init();
		void uninit();
		void update(float _dt);
		b2Body* create_hero_physic_body();
		b2Body* create_enemy_physic_body(const math::vec2& _pos, const math::vec2& _velocity);
		void release(b2Body*);
	private:
		b2World* m_p_world;
	};
}
#endif