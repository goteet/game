#ifndef LIB_GAME_CHARACTER_H
#define LIB_GAME_CHARACTER_H

#include "math_inc.h"
#include "Box2D/Box2D.h"
namespace game
{
	struct Character
	{
		Character();
		virtual ~Character();
		math::vec2 get_position();
		
		b2Body* m_body;
	};

	struct Enemy : public Character
	{
		void update();
		bool is_dead();		
	};
}
#endif 