#ifndef LIB_GAME_GAME_H
#define LIB_GAME_GAME_H

#include <vector>
#include "physical_world.h"

namespace game
{
	class Timer;
	struct Character;
	struct Enemy;

	class Game
	{
	public:
		Game(int _scene_width, int _scene_height);

		void init();
		void uninit();
		void update();

		enum e_finish_state{ WIN, LOSE, DUEL, RUN };
		e_finish_state is_finished();

	private:
		
		void initial_physical_world();
		void initial_player_hero();
		void initial_enemy_timer();

		void destroy_physical_world();
		void destroy_enemy_timer();
		void destroy_player_hero();
		void clear_enemy_list();

		void generate_enemies();
		void update_enemies_state();
		bool hittest_enemies_and_hero();
		bool is_generated_all_enemy();

		int	get_enemy_count();
	private:
		Timer* m_timer_enemy;
		int m_enemy_wave;

	public:
		Character* m_hero;
		typedef std::vector<Enemy*> EnemyVec;
		typedef EnemyVec::iterator EnemyIter;
		EnemyVec m_enemy_list;

		PhysicalWorld m_physical_world;

	};
}
#endif 