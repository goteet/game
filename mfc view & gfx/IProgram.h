#pragma once

namespace graphic{
	class Device;
}

enum {
	SHOW_BALLOON,
	RAIN_CHANGE_SPD,
	RAIN_CHANGE_SCALE_U,
	RAIN_CHANGE_V_FAC,
	RAIN_SAVE_DATA,
	RAIN_LOAD_DATA,
};

extern void GetCursor(int& x, int&y);
extern void SetCursor(int x,	int y);
extern void ShowCursor();
extern void HideCursor();
extern int GetWidth();
extern int GetHeight();

class IProgram
{
public:
	static IProgram* Create();

public:
	virtual void Init(graphic::Device* _pDevice) = 0;
	virtual void Deinit() = 0;
	virtual void RenderScene() = 0;
	virtual void Release() = 0;	
	
	virtual void OnResize(int width, int height) = 0;

	virtual void OnCommand(int ID, int arg1, int arg2){}
	virtual void* GetCommand(int ID, int arg1 = 0, int arg2 = 0){return 0;};
	virtual void OnMouseInput(int message, int flag, int x, int y, int d){}
	virtual void ShowBalloon(int arg1, int arg2, char* url){}

public:
	virtual void OnLStartDragging(int x, int y,UINT flag){};
	virtual void OnLStopDragging(int x, int y,UINT flag){};
	virtual void OnLDragging(int x, int y,UINT flag){};

	virtual void OnRStartDragging(int x, int y,UINT flag){};
	virtual void OnRStopDragging(int x, int y,UINT flag){};
	virtual void OnRDragging(int x, int y,UINT flag){};

	virtual void OnMouseWheel(int length, UINT flag){};
};


