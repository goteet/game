#ifndef GFX_SPARK_H
#define GFX_SPARK_H

#include "BaseInstance.h"


class SparkInstance : public BaseInstance
{
	TimeLine	m_TimeLine;
	Material	m_Material;

	int			m_RndSeed;
	float		m_CurLife;

public:
	DECL_DATA_ENTRY

	virtual void		Update(float dTime);
	virtual void		AddToRender();
	virtual Material*	GetMaterial();
	virtual TimeLine*	GetTimeLine();
	virtual bool		IsDead();
	virtual void		Reborn();
	virtual void		Release();

	Math::vec3	m_Position;

	int			m_SparkCnt;	
	Math::vec3	m_GenRange;

	Math::vec4	m_Velocity;			//w for rotation
	Math::vec4	m_Acceleration;		//w for rotation
	Math::vec2	m_Radial;			//x=vel  y=acc

	float		m_LifeShift;
	float		m_MaxLife;
	int			m_bAutoReborn;
	float		m_bRebornTime;

public:
	SparkInstance();
	
	float		GetLife();
	int			GetSeed();
	
	
	void InitSeed();

};
#endif