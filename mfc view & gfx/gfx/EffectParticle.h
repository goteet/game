#pragma once

#include "include/shader.h"
class EffectParticle : public graphic::IShaderEffect
{
public:
	virtual void OnDestroy();
	virtual void GetParams();
	virtual const char* GetVertexShader();
	virtual const char* GetPixelShader();
	virtual graphic::AttribLoc* GetAttribLoc();
	virtual int	GetAttribLocCount();

	graphic::ShaderParam* m_pMatWorld;
};