#ifndef GFX_DATA_ENTRY_H
#define GFX_DATA_ENTRY_H

#include "math_inc.h"
#include "variant.h"
inline int _type_to_n( int )		{ return Variant::VT_INT;	}
inline int _type_to_n( float )		{ return Variant::VT_FLOAT;	}

inline int _type_to_n( Math::vec2 )	{ return Variant::VT_VEC2;	}
inline int _type_to_n( Math::vec3 )	{ return Variant::VT_VEC3;	}
inline int _type_to_n( Math::vec4 )	{ return Variant::VT_VEC4;	}
//inline int _type_to_n( Math::mat4 )	{ return Variant::VT_MAT;	}

struct DATA_ENTRY{
	DWORD		nType;
	DWORD		nOffset;
	const char*	szName;
};

#define DECL_DATA_ENTRY		virtual const DATA_ENTRY* GetDataEntry() const;

#define BEGIN_DATA_ENTRY(Class) const DATA_ENTRY* Class::GetDataEntry() const\
{\
	static DATA_ENTRY Entry[] = {

#define END_DATA_ENTRY { 0 , 0 , 0 }\
	};\
	return Entry;\
}

#define DEF_DATA_ENTRY(C,N,T)			{_type_to_n(T()),offsetof(C,N),#N},
#define DEF_DATA_ENTRY_RENAME(C,N,R,T)	{_type_to_n(T()),offsetof(C,N),#R},

#endif