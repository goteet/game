#ifndef GFX_BASE_INSTANCE_H
#define GFX_BASE_INSTANCE_H

#include "DataEntry.h"
#include "Material.h"
#include "Timeline.h"
class BaseInstance
{
public:
	DECL_DATA_ENTRY

	virtual ~BaseInstance(){}

public:
	virtual void		Update(float dTime)	= 0;
	virtual void		AddToRender()		= 0;
	virtual Material*	GetMaterial()		= 0;
	virtual TimeLine*	GetTimeLine()		= 0;
	virtual bool		IsDead()			= 0;
	virtual void		Reborn()			= 0;
	virtual void		Release()			= 0;
public:
	virtual bool		SetParam(const char* prop, const Variant& _Value );
	virtual bool		GetParam(const char* prop, Variant& Value_)  const;
};


#endif
