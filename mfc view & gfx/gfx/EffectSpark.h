#pragma once

#include "include/shader.h"
class EffectSpark : public graphic::IShaderEffect
{
public:
	virtual void OnDestroy();
	virtual void GetParams();
	virtual const char* GetVertexShader();
	virtual const char* GetPixelShader();
	virtual graphic::AttribLoc* GetAttribLoc();
	virtual int	GetAttribLocCount();

	graphic::ShaderParam* m_pMatWorld;

	graphic::ShaderParam* m_pLife;
	graphic::ShaderParam* m_pGenRange;
	graphic::ShaderParam* m_pVelocity;
	graphic::ShaderParam* m_Acceleration;
	graphic::ShaderParam* m_Radial;	
};



