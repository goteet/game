#include "stdafx.h"
#include "Material.h"


Material::Material()
{
	SetSize(2,2);
}

bool Material::SetSize( int row, int col )
{

	if(row * col > MAX_CELL)
		return false;

	m_Count = row * col;
	m_Col = col;
	m_Row = row;
	m_CellCnt = Math::vec3(1.0f/row, 1.0f/col, m_Count-0.001);
	RebuildUVTable();
	return true;
}

void Material::RebuildUVTable()
{
	for(int i = 0; i < m_Row; i++)
	{
		for(int j = 0; j < m_Col; j++)
		{
			m_UVTable[i * m_Col + j] = Math::vec2(j * m_CellCnt.y, (m_Row-i-1) * m_CellCnt.x);
		}
	}
}


float* Material::GetUVTable()
{
	return m_UVTable->v;
}

int Material::GetUVCellCount()
{
	return m_Count;
}

float* Material::GetUVCell()
{
	return m_CellCnt.v;
}
