#include "stdafx.h"
#include "Particle.h"

BEGIN_DATA_ENTRY(ParticleInstance)
	DEF_DATA_ENTRY(ParticleInstance, m_Position, Math::vec3)
	DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_Type, int)
	DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_EmittRange, Math::vec4)
	DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_AccFix, Math::vec3)
	DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_AccRnd, Math::vec3)
	DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_VelFix, Math::vec3)
	DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_VelRadial, Math::vec3)
	DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_VelRnd, Math::vec3)
	DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_RotFix, float)
	DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_RotRnd, float)
	DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_LifeRange, Math::vec2)
	DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_GenInit, int)
	DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_GenPerSec, float)	
END_DATA_ENTRY

#include "RandFunc.h"
namespace{
	Math::vec3 GetRandPos( ParticleEmitter::EmitterType type, Math::vec4 range )
	{
		switch(type)
		{
		default:
		case ParticleEmitter::EMIIT_RING:
			{
				Math::vec3 pos(
					GetRandRange(-1.0f,1.0f),
					GetRandRange(-1.0f,1.0f),
					GetRandRange(-1.0f,1.0f)
					);

				pos.Normalize();
				pos *= Math::vec3(range);

				return pos;
			}
		case ParticleEmitter::EMIIT_BALL:
			{
				Math::vec3 pos(
					GetRandRange(-1.0f,1.0f),
					GetRandRange(-1.0f,1.0f),
					GetRandRange(-1.0f,1.0f)
					);

				while(pos.x * pos.x + pos.y * pos.y + pos.z * pos.z > 1.0f)
				{
					pos.x = GetRandRange(-1.0f,1.0f);
					pos.y = GetRandRange(-1.0f,1.0f);
					pos.z = GetRandRange(-1.0f,1.0f);
				}
				pos *= Math::vec3(range);

				return pos;
			}

		case ParticleEmitter::EMIIT_CUBE:
			return Math::vec3(
				GetRandRange(-range.x,range.x),
				GetRandRange(-range.y,range.y),
				GetRandRange(-range.z,range.z)
				);
		}
	}
}


bool Particle::IsDead()
{
	return (m_CurLife >= m_MaxLife);
}

void ParticleEmitter::Update(vector<Particle>& particles, float dTime)
{
	m_dTime += dTime;
	int nGenCnt = static_cast<int>(m_dTime * m_GenPerSec);
	if(nGenCnt != 0)
	{
		int index = particles.size();
		particles.resize(particles.size() + nGenCnt);
		int nParticleCnt = particles.size();
		for(int i = index; i != nParticleCnt; i++)
		{
			InitParticle(&particles[i]);
		}
		m_dTime = 0;
	}



		int nParticleCnt = particles.size();
		for(int i = 0; i != nParticleCnt; i++)
		{
			particles[i].m_CurLife += dTime;
			
			if(particles[i].IsDead())
			{
				
				particles[i] = particles.back();
				particles.pop_back();
				nParticleCnt--;
				
				i--;
				continue;
			}
			UpdateParticle(&particles[i], dTime);			
		}
}

ParticleEmitter::ParticleEmitter()
	: m_Type(EMIIT_BALL)
	, m_EmittRange(0,0,0,0)

	, m_AccFix(0, 0.5, 0)
	, m_AccRnd(0,0.25,0)

	, m_RotFix(0)
	, m_RotRnd(0)

	, m_VelFix(0,0,0)
	, m_VelRadial(0,0,0)

	, m_VelRnd(0,0,0)
	, m_LifeRange(2.0f, 6.5f)
	, m_GenPerSec(128)
	, m_GenInit(100)
	, m_dTime(0)
{
	
}

void ParticleEmitter::InitParticle( Particle* pParticle)
{
	pParticle->m_CurLife	= 0.0f;
	pParticle->m_MaxLife	= GetRandRange(m_LifeRange.x, m_LifeRange.y);

	Math::vec3  VelocityRnd	= Math::vec3(
		GetRandRange(-m_VelRnd.x,m_VelRnd.x),
		GetRandRange(-m_VelRnd.y,m_VelRnd.y),
		GetRandRange(-m_VelRnd.z,m_VelRnd.z)
		);

	Math::vec3  AccelerateRnd	= Math::vec3(
		GetRandRange(-m_AccRnd.x,m_AccRnd.x),
		GetRandRange(-m_AccRnd.y,m_AccRnd.y),
		GetRandRange(-m_AccRnd.z,m_AccRnd.z)
		);

	pParticle->m_Velocity	= m_VelFix + VelocityRnd;
	pParticle->m_AccVel		= m_AccFix + AccelerateRnd;
	pParticle->m_VelRot		= m_RotFix + GetRandRange(-m_RotRnd,m_RotRnd);

	pParticle->m_Vertex.pos	= GetRandPos(m_Type, m_EmittRange);
	pParticle->m_Vertex.rot	= 0.0f;
}


void ParticleEmitter::UpdateParticle( Particle* pParticle, float dTime)
{
	pParticle->m_Velocity	+= pParticle->m_AccVel * dTime;
	Math::vec3 vel			= m_VelRadial * pParticle->m_Vertex.pos.GetNormal();

	pParticle->m_Vertex.pos		+= (pParticle->m_Velocity + vel) * dTime;
	pParticle->m_Vertex.rot.x	+= pParticle->m_VelRot * dTime;
	pParticle->m_Vertex.rot.y	= pParticle->m_CurLife / pParticle->m_MaxLife;
}



ParticleInstance::ParticleInstance()
	: m_Position(0, 0, -20)
	, m_bInit(false)
{
	m_Material.m_Tex = "../res/rain32.bmp";
	m_Material.SetSize(1,1);
}
void ParticleInstance::Update(float dTime)
{
	if(!m_bInit)
	{
		m_Particles.resize(m_Emitter.m_GenInit);
		for(int i = 0; i != m_Emitter.m_GenInit; i++)
		{
			m_Emitter.InitParticle(&m_Particles[i]);
		}
		m_bInit = true;
	}
	m_Emitter.Update(m_Particles, dTime);
}



vector<Particle>* ParticleInstance::GetParticles()
{
	return &m_Particles;
}

Material* ParticleInstance::GetMaterial()
{
	return &m_Material;
}

TimeLine* ParticleInstance::GetTimeLine()
{
	return &m_TimeLine;
}

#include "RenderParticle.h"
void ParticleInstance::AddToRender()
{
	RenderParticle::instance()->Push(this);
}

bool ParticleInstance::IsDead()
{
	if(m_Particles.size() != 0
	|| m_Emitter.m_GenPerSec != 0)
		return false;
	return true;
}

void ParticleInstance::Reborn()
{
	//do nothing!
	if(m_Emitter.m_GenPerSec == 0)
		m_Emitter.m_GenPerSec = 128;
}

void ParticleInstance::Release()
{
	delete this;
}
