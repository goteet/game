#ifndef GFX_VARIANT_H
#define GFX_VARIANT_H

#include "math_inc.h"

class Variant
{
	/*
	template< typename A , typename B > struct max_size
	{
		static const int S = sizeof( A ) > sizeof( B ) ? sizeof( A ) : sizeof( B );
	};
	*/
	static const int S = sizeof( Math::mat4 );
	char	vBuf[ S ];
	int		nType;

public:

	enum
	{
		VT_NULL,
		VT_INT,		VT_FLOAT,
		VT_VEC2,	VT_VEC3,	VT_VEC4,		
		VT_MAT,
	};

	Variant();

	Variant& operator = ( const Variant& );
	Variant( const Variant& );

	Variant( int _Value );
	Variant( float _Value );
	Variant( const Math::vec2& _Value);
	Variant( const Math::vec3& _Value);
	Variant( const Math::vec4& _Value);
	Variant( const Math::mat4& _Value);

	int		GetType() const;
	void	SetType(int type);

public:
	int					GetInt()	const;
	float				GetFloat()	const;
	const Math::vec2&	GetVec2()	const;
	const Math::vec3&	GetVec3()	const;
	const Math::vec4&	GetVec4()	const;
	const Math::mat4&	GetMat()	const;

public:
	void SetNull();
	void Set( int _Value );
	void Set( float _Value );
	void Set( const Math::vec2& _Value);
	void Set( const Math::vec3& _Value);
	void Set( const Math::vec4& _Value);
	void Set( const Math::mat4& _Value);

public:
	void ReadMemory( const void* _pAddr );
	void WriteMemory( void* _pAddr ) const;

};
#endif