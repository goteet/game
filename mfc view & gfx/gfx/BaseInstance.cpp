#include "stdafx.h"
#include "BaseInstance.h"

BEGIN_DATA_ENTRY(BaseInstance)
END_DATA_ENTRY

bool BaseInstance::SetParam( const char* prop, const Variant& _Value )
{
	const DATA_ENTRY* pEntry = GetDataEntry();
	while( pEntry->szName )
	{
		if( strcmp( pEntry->szName , prop ) == 0 )
		{
			if( _Value.GetType() == pEntry->nType )
			{
				_Value.WriteMemory( (void*)((char*)(this)+pEntry->nOffset) );
				return true;
			}
		}
		pEntry++;
	}
	return false;
}

bool BaseInstance::GetParam( const char* prop, Variant& Value_ ) const
{
	const DATA_ENTRY* pEntry = GetDataEntry();
	while( pEntry->szName )
	{
		if( strcmp( pEntry->szName , prop ) == 0 )
		{
			Value_.SetType(pEntry->nType);
			Value_.ReadMemory( (void*)((char*)(this)+pEntry->nOffset) );
			return true;
		}
		pEntry++;
	}
	return false;
}
