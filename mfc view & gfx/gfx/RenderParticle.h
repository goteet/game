#ifndef GFX_RENDER_PARTICLE_H
#define GFX_RENDER_PARTICLE_H

#include <vector>
#include "device.h"
#include "EffectParticle.h"
#include "RenderBase.h"

class ParticleInstance;
class Particle;
struct VertexPTX;

namespace graphic{
	class VertexBuffer;
	class VertexBuffer;
	class IndexBuffer;
	class VertexDecl;
}
class RenderParticle : public RenderBase
{
public:
	static RenderParticle* instance();
	void Push(ParticleInstance* data);

protected:
	virtual void DoRender(float* matView, float* matProj);
	virtual bool InitRender();
	virtual void DeinitRender();

private:
	enum{ PARTICLE_CNT=4096, PTX_BUFFER_SIZE = PARTICLE_CNT*4, };

	graphic::VertexBuffer*	m_pSVBParticle;
	graphic::VertexBuffer*	m_pDVBParticle;
	graphic::IndexBuffer*	m_pIBParticle;
	graphic::VertexDecl*	m_pDeclParticle;

	void InitVBParticle();
	void InitIBParticle();
	void InitDeclParticle();

	std::vector<ParticleInstance*> m_vecParticles;

	void UpdatePos(Particle* pParticleVTX, int cnt);
};
#endif 

