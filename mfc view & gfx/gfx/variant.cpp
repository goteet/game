#include "stdafx.h"
#include "variant.h"

//#pragma warning ( disable : 4996 )

namespace
{
	template< typename T > void Assign(void* _pDst ,const void* _pSrc )
	{ 
		*(T*)_pDst = *(T*)_pSrc;
	}
	const char vEmptyBuffer[ sizeof(Variant) ] = { 0 };
}

Variant::Variant()
{
	nType = VT_NULL;
	memset(vBuf,0,sizeof(vBuf));
}
Variant& Variant::operator = ( const Variant& _V )
{
	memcpy(vBuf,_V.vBuf,sizeof(vBuf));
	nType = _V.nType;
	return *this;
}

Variant::Variant( const Variant& _V )
{
	
	memcpy(vBuf,_V.vBuf,sizeof(vBuf));
	nType = _V.nType;
}

int Variant::GetType() const
{ 
	return nType;
}



void Variant::SetType( int type )
{
	nType = type;
}



int Variant::GetInt() const
{
	return nType == VT_INT ? *(int*)vBuf : 0;
}

float Variant::GetFloat() const 
{
	return nType == VT_FLOAT ? *(float*)vBuf : 0.0f;
}

const Math::vec2& Variant::GetVec2() const
{
	return nType == VT_VEC2 ? *(Math::vec2*)vBuf : *(Math::vec2*)vEmptyBuffer;
}
const Math::vec3& Variant::GetVec3() const
{ 
	return nType == VT_VEC3 ? *(Math::vec3*)vBuf : *(Math::vec3*)vEmptyBuffer;
}
const Math::vec4& Variant::GetVec4() const
{ 
	return nType == VT_VEC4 ? *(Math::vec4*)vBuf : *(Math::vec4*)vEmptyBuffer;
}

const Math::mat4& Variant::GetMat() const
{ 
	return nType == VT_MAT ? *(Math::mat4*)vBuf : *(Math::mat4*)vEmptyBuffer;
}


void Variant::SetNull()
{
	nType = VT_NULL;
	memset(vBuf,0,sizeof(vBuf));
}

void Variant::Set( int _Value )
{
	nType = VT_INT;
	*(int*)vBuf = _Value;
}

void Variant::Set( float _Value )
{
	nType = VT_FLOAT;
	*(float*)vBuf = _Value;
}

void Variant::Set( const Math::vec2& _Value)
{
	nType = VT_VEC2;
	*(Math::vec2*)vBuf = _Value;
}

void Variant::Set( const Math::vec3& _Value)
{
	nType = VT_VEC3;
	*(Math::vec3*)vBuf = _Value;
}

void Variant::Set( const Math::vec4& _Value)
{
	nType = VT_VEC4;
	*(Math::vec4*)vBuf = _Value;
}

void Variant::Set( const Math::mat4& _Value )
{
	nType = VT_MAT;
	*(Math::mat4*)vBuf = _Value;
}

void Variant::WriteMemory( void* _pAddr ) const
{
	switch( nType )
	{
	case	VT_INT:			Assign<int>(_pAddr,vBuf);			break;
	case	VT_FLOAT:		Assign<float>(_pAddr,vBuf);			break;
	case	VT_VEC2:		Assign<Math::vec2>(_pAddr,vBuf);	break;
	case	VT_VEC3:		Assign<Math::vec3>(_pAddr,vBuf);	break;
	case	VT_VEC4:		Assign<Math::vec4>(_pAddr,vBuf);	break;
	case	VT_MAT:			Assign<Math::mat4>(_pAddr,vBuf);	break;
	};
}

void Variant::ReadMemory( const void* _pAddr )
{
	switch( nType )
	{
	case	VT_INT:			Set((*(int*)_pAddr));			break;
	case	VT_FLOAT:		Set((*(float*)_pAddr));			break;
	case	VT_VEC2:		Set((*(Math::vec2*)_pAddr));	break;
	case	VT_VEC3:		Set((*(Math::vec3*)_pAddr));	break;
	case	VT_VEC4:		Set((*(Math::vec4*)_pAddr));	break;
	case	VT_MAT:			Set((*(Math::mat4*)_pAddr));	break;
	};
}

Variant::Variant( int _Value )
{
	nType = VT_INT;
	*(int*)vBuf = _Value;
}

Variant::Variant( float _Value )
{
	nType = VT_FLOAT;
	*(float*)vBuf = _Value;
}

Variant::Variant( const Math::vec2& _Value)
{
	nType = VT_VEC2;
	*(Math::vec2*)vBuf = _Value;
}

Variant::Variant( const Math::vec3& _Value)
{
	nType = VT_VEC3;
	*(Math::vec3*)vBuf = _Value;
}

Variant::Variant( const Math::vec4& _Value)
{
	nType = VT_VEC4;
	*(Math::vec4*)vBuf = _Value;
}

Variant::Variant( const Math::mat4& _Value )
{
	nType = VT_MAT;
	*(Math::mat4*)vBuf = _Value;
}