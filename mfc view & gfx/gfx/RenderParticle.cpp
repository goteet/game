#include "stdafx.h"
#include "RenderParticle.h"
#include "device.h"
#include "math_inc.h"
#include "Particle.h"
#include "include/device.h"
RenderParticle* RenderParticle::instance()
{
	static RenderParticle inst;
	return &inst;
}
bool RenderParticle::InitRender()
{
	InitVBParticle();
	InitIBParticle();
	InitDeclParticle();

	m_pEffect = new EffectParticle();
	m_pEffect->Init(m_pDevice);

	return true;
}


void RenderParticle::DeinitRender()
{
	m_pSVBParticle->Release();
	m_pDVBParticle->Release();
	m_pIBParticle->Release();
	m_pDeclParticle->Release();

}

void RenderParticle::Push( ParticleInstance* data )
{
	m_vecParticles.push_back(data);
}

void RenderParticle::DoRender(float* matView, float* matProj)
{
	int nParticleCnt = m_vecParticles.size();
	for(int i = 0; i != nParticleCnt; i++)
	{
		ParticleInstance* inst = m_vecParticles[i];
		std::vector<Particle>* pParticles = inst->GetParticles();

		if(pParticles->size() == 0)
			continue;


		Particle*	pVertex = &((*pParticles)[0]);
		int pCnt = pParticles->size();

		SetMatertial(inst->GetMaterial());
		SetTimeLine(inst->GetTimeLine());


		EffectParticle* pEffect = (EffectParticle*)m_pEffect;
		Math::mat4 matWorld = Math::translate(inst->m_Position);
		if(pEffect->m_pMatWorld)	pEffect->m_pMatWorld->SetMatrix(matWorld.mp);


		while(pCnt > 0)
		{
			int c = PARTICLE_CNT;
			if(pCnt < PARTICLE_CNT)
			{
				c = pCnt;
			}

			UpdatePos(pVertex, c);
			pParticles += c;
			pCnt-=c;


			m_pDevice->SetVertexBuffer(0, m_pSVBParticle);
			m_pDevice->SetVertexBuffer(1, m_pDVBParticle);
			m_pDevice->SetVertexDecl(m_pDeclParticle);
			m_pDevice->SetIndexBuffer(m_pIBParticle);
			m_pDevice->DrawIndexed(graphic::PT_TRIANGLES, 0, c * 6);
		}
		
	}
	m_vecParticles.clear();
}

namespace {
	struct VertexSParticle{
		Math::vec4 coord_uv;
	};

	struct VertexRing{
		Math::vec3 pos;
	};
}


void RenderParticle::InitVBParticle()
{
	const int VB_SIZE	= PARTICLE_CNT * 4;
	const int VB_SIZE_S = VB_SIZE * sizeof(VertexSParticle);
	const int VB_SIZE_D = VB_SIZE * sizeof(VertexPTX);
	m_pSVBParticle = m_pDevice->CreateVertexBuffer(VB_SIZE_S, graphic::BU_STATIC_DRAW);
	m_pDVBParticle = m_pDevice->CreateVertexBuffer(VB_SIZE_D, graphic::BU_DYNAMIC_DRAW);

	VertexSParticle* pSVBParticle = (VertexSParticle*)m_pSVBParticle->Lock(graphic::BA_WRITE_ONLY);
	for(int i = 0; i != PARTICLE_CNT; i++)
	{
		int VOffset = i*4;
		VertexSParticle* pVertexFix = pSVBParticle + VOffset;

		/*	0 1
			2 3	*/
		pVertexFix[0].coord_uv.x = -0.5f;	pVertexFix[0].coord_uv.y = 0.5f;
		pVertexFix[1].coord_uv.x = 0.5f;	pVertexFix[1].coord_uv.y = 0.5f;
		pVertexFix[2].coord_uv.x = -0.5f;	pVertexFix[2].coord_uv.y = -0.5f;
		pVertexFix[3].coord_uv.x = 0.5f;	pVertexFix[3].coord_uv.y = -0.5f;

		pVertexFix[0].coord_uv.z = 0; pVertexFix[0].coord_uv.w = 0;
		pVertexFix[1].coord_uv.z = 1; pVertexFix[1].coord_uv.w = 0;
		pVertexFix[2].coord_uv.z = 0; pVertexFix[2].coord_uv.w = 1;
		pVertexFix[3].coord_uv.z = 1; pVertexFix[3].coord_uv.w = 1;
	}
	m_pSVBParticle->Unlock();
}

void RenderParticle::InitIBParticle()
{
	const int IB_SIZE = PARTICLE_CNT * 2 * 3 * sizeof(unsigned short);
	m_pIBParticle = m_pDevice->CreateIndexBuffer(IB_SIZE, graphic::BU_STATIC_DRAW);

	unsigned short* pIBParticle = (unsigned short*)m_pIBParticle->Lock(graphic::BA_WRITE_ONLY);

	for(int i = 0; i != PARTICLE_CNT; i++)
	{
		int IOffset = i * 6;
		int VOffset = i*4;
		unsigned short* pIndex = pIBParticle+IOffset;

		/*	0 1
			2 3	*/
		pIndex[0] = 0+VOffset;
		pIndex[1] = 2+VOffset;
		pIndex[2] = 1+VOffset;

		pIndex[3] = 1+VOffset;
		pIndex[4] = 2+VOffset;
		pIndex[5] = 3+VOffset;
	}
	m_pIBParticle->Unlock();
}

void RenderParticle::InitDeclParticle()
{
	const int DECL_SIZE=3;
	graphic::AttribDecl declArray[DECL_SIZE];

	int IDX = 0;
	declArray[IDX].index = 0;
	declArray[IDX].location = 0;
	declArray[IDX].normalize = false;
	declArray[IDX].offset = offsetof(VertexSParticle, coord_uv);
	declArray[IDX].strip = sizeof(VertexSParticle);
	declArray[IDX].type = graphic::DT_FLOAT4;

	IDX = 1;
	declArray[IDX].index = 1;
	declArray[IDX].location = 1;
	declArray[IDX].normalize = false;
	declArray[IDX].offset = offsetof(VertexPTX, pos);
	declArray[IDX].strip = sizeof(VertexPTX);
	declArray[IDX].type = graphic::DT_FLOAT3;

	IDX = 2;
	declArray[IDX].index = 1;
	declArray[IDX].location = 2;
	declArray[IDX].normalize = false;
	declArray[IDX].offset = offsetof(VertexPTX, rot);
	declArray[IDX].strip = sizeof(VertexPTX);
	declArray[IDX].type = graphic::DT_FLOAT3;

	m_pDeclParticle = m_pDevice->CreateVertexDecl(declArray, DECL_SIZE);

}

void RenderParticle::UpdatePos(Particle* pParticle, int cnt)
{
	static VertexPTX  VTX_BUFFER[PTX_BUFFER_SIZE];
	//
	//	map buffer is toooooooo slow !
	//
	//VertexPTX* pDVBParticle = (VertexPTX*)m_pDVBParticle->Lock(Graphic::BA_WRITE_ONLY);
	
	for(int i = 0; i != cnt; i++)
	{
		int VOffset = i*4;
		//VertexPTX* pVertex = pDVBParticle + VOffset;
		VertexPTX* pVertex = VTX_BUFFER + VOffset;
			
		memcpy(pVertex+0, &(pParticle->m_Vertex), sizeof(VertexPTX));
		memcpy(pVertex+1, &(pParticle->m_Vertex), sizeof(VertexPTX));
		memcpy(pVertex+2, &(pParticle->m_Vertex), sizeof(VertexPTX));
		memcpy(pVertex+3, &(pParticle->m_Vertex), sizeof(VertexPTX));

		pParticle++;
	}

	m_pDVBParticle->Upload((const char*)VTX_BUFFER, cnt*4*sizeof(VertexPTX));
	
	//m_pDVBParticle->Unlock();
	
}
