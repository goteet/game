#ifndef GFX_TIME_LINE_H
#define GFX_TIME_LINE_H

#include "math_inc.h"
enum {MAX_KEY_NODE = 8, LAST_KEY_NODE = MAX_KEY_NODE-1,};

template<class TYPE>
class KeyLine
{
public:
	TYPE	GetValue(float time)
	{
		if(time  < 0.0f)
			return GetKey(0);
		else if(time > 1.0f)
			return GetKey(LAST_KEY_NODE);
		else
		{
			time *= MAX_KEY_NODE;
			int prevKey = (int)(time);
			float p = time - prevKey;
			return lerp(
				GetKey(prevKey),
				GetKey(prevKey+1),
				p);
		}
	}

	void	SetKey(int index, TYPE value)
	{
		if(index >= 0 && index  <= LAST_KEY_NODE)	KeyValue[index] = value;
	}
	void	SetCount(int cnt)
	{
		if(cnt < 1)	cnt = 1;
		else if(cnt > MAX_KEY_NODE) cnt = MAX_KEY_NODE;
		KeyCount = cnt;
	}
	TYPE	GetKey(int index)
	{
		if(index >= 0 && index  <= LAST_KEY_NODE)	return  KeyValue[index];
		return  KeyValue[0];
	}
	float*	GetPTR()	{ return (float*)KeyValue;}
	int		GetCount()	{ return KeyCount;}
	
	
private:
	TYPE	lerp(TYPE lhs, TYPE rhs, float p)
	{
		return lhs*(1.0f - p) + rhs*p;
	}
	
	TYPE	KeyValue[MAX_KEY_NODE];
	int		KeyCount;

};


class TimeLine
{
public:
	TimeLine();
	float*	GetColorPtr();
	float*	GetSizePtr();
	KeyLine<Math::vec4>*	GetColor();
	KeyLine<Math::vec2>*	GetSize();
private:
	KeyLine<Math::vec4>	m_Colors;
	KeyLine<Math::vec2>	m_Sizes;
};
#endif