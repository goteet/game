#include "stdafx.h"
#include "EffectSpark.h"

void EffectSpark::OnDestroy()
{
	if(m_pMatWorld)	m_pMatWorld->Release();

	if(m_pGenRange)	m_pGenRange->Release();
	if(m_pLife)		m_pLife->Release();

	if(m_pVelocity)	m_pVelocity->Release();
	if(m_Acceleration)m_Acceleration->Release();
	if(m_Radial)	m_Radial->Release();
}

void EffectSpark::GetParams()
{
	m_pMatWorld	= GetParam("u_MatWorld");

	m_pLife		= GetParam("u_Life");
	m_pGenRange	= GetParam("u_GenRange");

	m_pVelocity	= GetParam("u_Velocity");
	m_Acceleration= GetParam("u_Acceleration");
	m_Radial	= GetParam("u_Radial");	
}

const char* EffectSpark::GetVertexShader()
{
	static const char* s_szVertexShader = {
		"#version 110\n"
		"uniform vec4 u_CameraX;"  
		"uniform vec4 u_CameraY;"
		"uniform mat4 u_MatWorld;"
		"uniform mat4 u_MatView;"  
		"uniform mat4 u_MatProj;"
		"uniform vec3 u_GenRange;"
		"uniform vec4 u_Velocity;"
		"uniform vec4 u_Acceleration;"
		"uniform vec2 u_Radial;"
		"uniform vec2 u_Life;"
		"uniform vec3 u_CellSize;"
		"uniform vec2 u_KeyLines;"
		"uniform vec4 u_Colors[8];"
		"uniform vec2 u_Sizes[8];"
		"uniform vec2 u_UVTable[16];"
		"attribute vec4 in_Coord_UV;"
		"attribute vec4 in_Position;"
		"attribute vec4 in_Velocity;"
		"varying vec2 vTexUV;"
		"varying vec4 vColor;"
		"vec4 GetColor(float life){"
		"	life *= u_KeyLines.x;"
		"	int prevKey = int(floor(life));"
		"	float p = fract(life);"
		"	return mix(	u_Colors[prevKey],"
		"				u_Colors[prevKey+1],"
		"				p);"
		"}"
		"vec2 GetUV(vec2 uv, float life){"
		"	life *= u_CellSize.z;"
		"	int index = int(floor(life));"
		"	return uv * u_CellSize.yx + u_UVTable[index];"
		"}"
		"vec2 GetSize(float life){"
		"	life *= u_KeyLines.y;"
		"	int prevKey = int(floor(life));"
		"	float p = fract(life);"
		"	vec2 uv = in_Coord_UV.xy * mix(u_Sizes[prevKey], u_Sizes[prevKey+1], p);"
		"	return vec2(uv.x, uv.y);"
		"}"
		"vec3 GetBillBoardExt(float rotate, float life){"
		"	vec3 axisP	= u_CameraX.xyz;"
		"	vec3 axisQ	= u_CameraY.xyz;"
		"	float cosR	= cos(rotate);"
		"	float sinR	= sin(rotate);"
		"	vec3 newP	= axisP * cosR + axisQ * sinR;"
		"	vec3 newQ	= axisP * sinR - axisQ * cosR;"
		"	vec2 size	= GetSize(life);"
		"	vec3 ext	= newP * size.x + newQ * size.y;"
		"	return ext;"
		"}"
		"vec3 GetPos(vec3 pSeed, vec3 vSeed, float time){"
		"	vec3 pos	= pSeed * u_GenRange;"
		"	vec3 vel	= u_Velocity.xyz + u_Acceleration.xyz * 0.5 * time;"
		"	vec3 disp	= pos + vel * vSeed * time;"
		"	vec3 dirR	= vec3(0.0,0.0,0.0);"
		"	if(0.01 < length(disp)) dirR = normalize(disp);"
		"	vec3 velR	= (u_Radial.x + u_Radial.y * 0.5 * time) * dirR;"
		"	vec3 dispR	= time * velR;"
		"	pos			= disp + dispR * min(1.0, length(disp) / length(dispR) + step(0.0,dot(disp,dispR)) * 1000.0);"//�����һ��
		"	return pos;"
		"}"
		"void main(){"
		"	float life	= fract(in_Position.w * u_Life.y + u_Life.x + 1.0);"
		"	float rot	= life * in_Velocity.w * u_Velocity.w + u_Acceleration.w * life * 0.5;"
		"	vec3 posExt	= GetBillBoardExt(rot, life);"
		"	vec3 pModel	= GetPos(in_Position.xyz, in_Velocity.xyz, life);"
		"	vec4 posWorld = u_MatWorld* vec4(pModel,1.0) + vec4(posExt, 0.0);"
		"	gl_Position	= u_MatProj * u_MatView * posWorld;"
		"	vTexUV		= GetUV(in_Coord_UV.zw, life);"
		"	vColor		= GetColor(life);"
		"}\0\0"
	};
	return s_szVertexShader;
}

const char* EffectSpark::GetPixelShader()
{
	static const char* s_szFragShader = {
		"#version 110\n"   
		"uniform sampler2D u_Texture;"
		"varying vec2 vTexUV;"
		"varying vec4 vColor;"
		"void main(){"
		"   gl_FragColor = vColor * texture2D(u_Texture, vTexUV);"
		"}\0\0"  
	};
	return s_szFragShader;
}

graphic::AttribLoc* EffectSpark::GetAttribLoc()
{
	static graphic::AttribLoc loc[] =
	{
		
		{0,	"in_Coord_UV"},
		{1,	"in_Position"},
		{2,	"in_Velocity"},
	};
	return loc;
}

int EffectSpark::GetAttribLocCount()
{
	return 3;
}
