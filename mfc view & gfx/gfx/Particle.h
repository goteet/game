#ifndef GFX_PARTICLE_H
#define GFX_PARTICLE_H

#include "BaseInstance.h"
#include <vector>
using std::vector;

struct VertexPTX{
	Math::vec3 pos;
	Math::vec3 rot;		//x=rot, y=life
};


class Particle
{
public:
	bool IsDead();

	float m_MaxLife;
	float m_CurLife;

	Math::vec3	m_Velocity;
	Math::vec3	m_AccVel;
	float		m_VelRot;	

	VertexPTX	m_Vertex;
};

class ParticleEmitter
{
	
public:
	ParticleEmitter();
	enum EmitterType{
		EMIIT_CUBE,
		EMIIT_BALL,
		EMIIT_RING,
	};

public:
	void Update(vector<Particle>& particles, float dTime);
	void InitParticle(Particle* pParticle);
	void UpdateParticle(Particle* pParticle, float dTime);


	EmitterType m_Type;
	Math::vec4	m_EmittRange;

	Math::vec3	m_AccFix;
	Math::vec3	m_AccRnd;

	Math::vec3	m_VelFix;
	Math::vec3	m_VelRadial;
	Math::vec3	m_VelRnd;

	float		m_RotFix;
	float		m_RotRnd;

	Math::vec2	m_LifeRange;

	int			m_GenInit;
	float		m_GenPerSec;

private:
	float		m_dTime;
};

class ParticleInstance : public BaseInstance
{
	TimeLine			m_TimeLine;
	Material			m_Material;
	vector<Particle>	m_Particles;
public:
	DECL_DATA_ENTRY

	ParticleInstance();
	virtual void		Update(float dTime);
	virtual void		AddToRender();
	virtual Material*	GetMaterial();
	virtual TimeLine*	GetTimeLine();
	virtual bool		IsDead();
	virtual void		Reborn();
	virtual void		Release();

	vector<Particle>*	GetParticles();

	ParticleEmitter		m_Emitter;
	Math::vec3			m_Position;
	int					m_bInit;
};
#endif