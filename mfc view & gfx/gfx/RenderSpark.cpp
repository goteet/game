#include "stdafx.h"
#include "RenderSpark.h"
#include "device.h"
#include "math_inc.h"
#include "Spark.h"

#include "RandFunc.h"
#include "include/device.h"

namespace{
	Math::vec3 GetRandPos(float d = 0.5f)
	{
		return Math::vec3(
			GetRandRange(-d, d),
			GetRandRange(-d, d),
			GetRandRange(-d, d)
			);
	}
	Math::vec3 GetRandVelocity()
	{
		Math::vec3 velc(
			GetRandRange(-1.0f,1.0f),
			GetRandRange(-1.0f,1.0f),
			GetRandRange(-1.0f,1.0f)
			);

		while(velc.x * velc.x + velc.y * velc.y + velc.z * velc.z > 1.0f)
		{
			velc.x = GetRandRange(-1.0f,1.0f);
			velc.y = GetRandRange(-1.0f,1.0f);
			velc.z = GetRandRange(-1.0f,1.0f);
		}

		return velc;
	}
}

RenderSpark* RenderSpark::instance()
{
	static RenderSpark inst;
	return &inst;
}




bool RenderSpark::InitRender()
{
	InitVBSpark();
	InitIBSpark();
	InitDeclSpark();

	m_pEffect = new EffectSpark();
	m_pEffect->Init(m_pDevice);

	return true;
}


void RenderSpark::DeinitRender()
{
	m_pVBSparkFix->Release();
	for(int i = 0; i != 5; i++)	m_pVBSparkRnd[i]->Release();
	m_pIBSpark->Release();
	m_pDecl->Release();
}

void RenderSpark::Push( SparkInstance* data )
{
	m_vecSparks.push_back(data);
}

void RenderSpark::DoRender(float* matView, float* matProj)
{
	int nSparkCnt = m_vecSparks.size();
	for(int i = 0; i != nSparkCnt; i++)
	{
		SparkInstance* inst = m_vecSparks[i];

		int pCnt = inst->m_SparkCnt;

		SetMatertial(inst->GetMaterial());
		SetTimeLine(inst->GetTimeLine());


		Math::mat4 matWorld = Math::translate(inst->m_Position);
		//matWorld = Math::translate(50,0,0);

		EffectSpark* pEffect = (EffectSpark*)m_pEffect;
		if(pEffect->m_pMatWorld)	pEffect->m_pMatWorld->SetMatrix(matWorld.mp);
		if(pEffect->m_pGenRange)	pEffect->m_pGenRange->SetFloat3(inst->m_GenRange.v);
		if(pEffect->m_pLife)		pEffect->m_pLife->SetFloat2(inst->GetLife(), inst->m_LifeShift);
		if(pEffect->m_pVelocity)	pEffect->m_pVelocity->SetFloat4(inst->m_Velocity.v);
		if(pEffect->m_Acceleration)	pEffect->m_Acceleration->SetFloat4(inst->m_Acceleration.v);
		if(pEffect->m_Radial)		pEffect->m_Radial->SetFloat2(inst->m_Radial.v);

		

		while(pCnt > 0)
		{
			int c = SPARK_SIZE;
			if(pCnt < SPARK_SIZE)
			{
				c = pCnt;
			}

			pCnt-=c;

			m_pDevice->SetVertexBuffer(0, m_pVBSparkFix);
			m_pDevice->SetVertexBuffer(1, m_pVBSparkRnd[inst->GetSeed()]);
			m_pDevice->SetVertexDecl(m_pDecl);
			m_pDevice->SetIndexBuffer(m_pIBSpark);
			m_pDevice->DrawIndexed(graphic::PT_TRIANGLES, 0, c * 6);

		}
	}
	m_vecSparks.clear();
}

namespace {
	struct VertexSparkFix{
		Math::vec4 coord_uv;
	};

	struct VertexSparkRnd{
		Math::vec4 pos;		//w for life shift;
		Math::vec4 movedir;
	};

}
void RenderSpark::InitVBSpark()
{
	const int VB_SIZE	= SPARK_SIZE * 4;
	const int VB_SIZE_FIX = VB_SIZE * sizeof(VertexSparkFix);
	const int VB_SIZE_RND = VB_SIZE * sizeof(VertexSparkRnd);
	m_pVBSparkFix = m_pDevice->CreateVertexBuffer(VB_SIZE_FIX, graphic::BU_STATIC_DRAW);



	VertexSparkFix* pSVBSparkFix = (VertexSparkFix*)m_pVBSparkFix->Lock(graphic::BA_WRITE_ONLY);
	for(int i = 0; i != SPARK_SIZE; i++)
	{
		int VOffset = i*4;
		VertexSparkFix* pVertexFix = pSVBSparkFix + VOffset;

		/*	0 1
		2 3	*/
		pVertexFix[0].coord_uv.x = -0.5f;	pVertexFix[0].coord_uv.y = 0.5f;
		pVertexFix[1].coord_uv.x = 0.5f;	pVertexFix[1].coord_uv.y = 0.5f;
		pVertexFix[2].coord_uv.x = -0.5f;	pVertexFix[2].coord_uv.y = -0.5f;
		pVertexFix[3].coord_uv.x = 0.5f;	pVertexFix[3].coord_uv.y = -0.5f;

		pVertexFix[0].coord_uv.z = 0; pVertexFix[0].coord_uv.w = 0;
		pVertexFix[1].coord_uv.z = 1; pVertexFix[1].coord_uv.w = 0;
		pVertexFix[2].coord_uv.z = 0; pVertexFix[2].coord_uv.w = 1;
		pVertexFix[3].coord_uv.z = 1; pVertexFix[3].coord_uv.w = 1;
	}
	m_pVBSparkFix->Unlock();


	for(int rnd = 0; rnd != 5; rnd++)
	{
		m_pVBSparkRnd[rnd] = m_pDevice->CreateVertexBuffer(VB_SIZE_RND, graphic::BU_STATIC_DRAW);
		VertexSparkRnd* pSVBSparkRnd = (VertexSparkRnd*)m_pVBSparkRnd[rnd]->Lock(graphic::BA_WRITE_ONLY);
		for(int i = 0; i != SPARK_SIZE; i++)
		{
			int VOffset = i*4;
			VertexSparkRnd* pVertexRnd = pSVBSparkRnd + VOffset;

			pVertexRnd[0].pos = 
				pVertexRnd[1].pos =
				pVertexRnd[2].pos =
				pVertexRnd[3].pos = GetRandPos();

			pVertexRnd[0].pos.w = 
				pVertexRnd[1].pos.w =
				pVertexRnd[2].pos.w =
				pVertexRnd[3].pos.w = GetRandRange(-1.0f, 1.0f);

			pVertexRnd[0].movedir = 
				pVertexRnd[1].movedir =
				pVertexRnd[2].movedir =
				pVertexRnd[3].movedir = GetRandVelocity();

			//rotation
			pVertexRnd[0].movedir.w = 
				pVertexRnd[1].movedir.w =
				pVertexRnd[2].movedir.w =
				pVertexRnd[3].movedir.w = GetRandRange(-1.0f,1.0f);
		}

		m_pVBSparkRnd[rnd]->Unlock();
	}
}

void RenderSpark::InitIBSpark()
{
	const int IB_SIZE = SPARK_SIZE * 2 * 3 * sizeof(unsigned short);
	m_pIBSpark = m_pDevice->CreateIndexBuffer(IB_SIZE, graphic::BU_STATIC_DRAW);

	unsigned short* pIBSpark = (unsigned short*)m_pIBSpark->Lock(graphic::BA_WRITE_ONLY);

	for(int i = 0; i != SPARK_SIZE; i++)
	{
		int IOffset = i * 6;
		int VOffset = i*4;
		unsigned short* pIndex = pIBSpark+IOffset;

		/*	0 1
		2 3	*/
		pIndex[0] = 0+VOffset;
		pIndex[1] = 2+VOffset;
		pIndex[2] = 1+VOffset;

		pIndex[3] = 1+VOffset;
		pIndex[4] = 2+VOffset;
		pIndex[5] = 3+VOffset;
	}
	m_pIBSpark->Unlock();

}

void RenderSpark::InitDeclSpark()
{
	const int DECL_SIZE = 3;

	graphic::AttribDecl declArray[DECL_SIZE];
	int IDX = 0;
	declArray[IDX].index = 0;
	declArray[IDX].location = 0;
	declArray[IDX].normalize = false;
	declArray[IDX].offset = offsetof(VertexSparkFix, coord_uv);
	declArray[IDX].strip = sizeof(VertexSparkFix);
	declArray[IDX].type = graphic::DT_FLOAT4;

	IDX = 1;
	declArray[IDX].index = 1;
	declArray[IDX].location = 1;
	declArray[IDX].normalize = false;
	declArray[IDX].offset = offsetof(VertexSparkRnd, pos);
	declArray[IDX].strip = sizeof(VertexSparkRnd);
	declArray[IDX].type = graphic::DT_FLOAT4;

	IDX = 2;
	declArray[IDX].index = 1;
	declArray[IDX].location = 2;
	declArray[IDX].normalize = false;
	declArray[IDX].offset = offsetof(VertexSparkRnd, movedir);
	declArray[IDX].strip = sizeof(VertexSparkRnd);
	declArray[IDX].type = graphic::DT_FLOAT4;

	m_pDecl = m_pDevice->CreateVertexDecl(declArray, DECL_SIZE);

}
