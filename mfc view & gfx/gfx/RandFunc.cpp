#include "stdafx.h"
#include "RandFunc.h"

float GetRandRange( float min, float max )
{
	if(min > max)
	{//swap
		min = min + max;
		max = min - max;
		min = min - max;
	}
	return (rand() * (max - min)/ RAND_MAX + min);
}