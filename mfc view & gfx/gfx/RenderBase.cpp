#include "stdafx.h"
#include "RenderBase.h"


#include "Material.h"
#include "TimeLine.h"
#include "../TextureManager.h"

#include "include/shader.h"
#include "include/device.h"


void RenderBase::Render( float* matView, float* matProj )
{
	m_pEffect->Begin();
	m_pDevice->SetRenderStage(graphic::RS_BLENDING, true);
	m_pDevice->SetRenderStage(graphic::RS_DEPETH_TEST, false);
	glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
	//matrix setting
	if(m_pMatView)	m_pMatView->SetMatrix(matView);
	if(m_pMatProj)	m_pMatProj->SetMatrix(matProj);
	SetCameraParam(matView);

	DoRender(matView, matProj);

	m_pDevice->SetRenderStage(graphic::RS_DEPETH_TEST, true);
	m_pDevice->SetRenderStage(graphic::RS_BLENDING, false);
	m_pEffect->End();

}

void RenderBase::SetTimeLine( TimeLine* timeline )
{
	if(m_pKeyLines)	m_pKeyLines->SetFloat2(
		timeline->GetColor()->GetCount() - 1,
		timeline->GetSize()->GetCount() - 1);

	if(m_pColors)	m_pColors->SetFloat4Array(timeline->GetColorPtr(), MAX_KEY_NODE);
	if(m_pSizes)	m_pSizes->SetFloat2Array(timeline->GetSizePtr(), MAX_KEY_NODE);
}

void RenderBase::SetMatertial( Material* matertial )
{
	if(matertial->m_Tex == "")
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	if(!TextureManager::Inst()->BindTexture(matertial->m_Tex.c_str()))
	{
		//TextureManager::Inst()->LoadTexture(matertial->m_Tex.c_str(), GL_BGR, GL_RGB);
		TextureManager::Inst()->LoadTexture(matertial->m_Tex.c_str(), GL_BGRA, GL_RGBA);
	}

	if(m_pTexture)	m_pTexture->SetTexture(0);
	if(m_pUVTable)	m_pUVTable->SetFloat2Array(matertial->GetUVTable(), matertial->GetUVCellCount());
	if(m_pUVCellCnt)m_pUVCellCnt->SetFloat3(matertial->GetUVCell());

}

void RenderBase::SetCameraParam( float* matView )
{
	Math::mat4 view = matView;
	view.Inverse();
	Math::vec4 camerax = Math::vec4(view.m[0][0], view.m[0][1], view.m[0][2], view.m[0][3]);
	Math::vec4 cameray = Math::vec4(view.m[1][0], view.m[1][1], view.m[1][2], view.m[1][3]);

	if(m_pCameraX)	m_pCameraX->SetFloat4(camerax.v);
	if(m_pCameraY)	m_pCameraY->SetFloat4(cameray.v);
}

bool RenderBase::Init( graphic::Device* device )
{
	if(!device)
		return false;
	m_pDevice = device;

	bool bIsOK = InitRender();
	if(bIsOK)
	{
		InitParameter();
	}
	
	return bIsOK;
}

void RenderBase::Deinit()
{
	DeinitParameter();
	DeinitRender();
	delete m_pEffect;
}

void RenderBase::InitParameter()
{
	m_pCameraX = m_pEffect->GetParam("u_CameraX");
	m_pCameraY = m_pEffect->GetParam("u_CameraY");


	m_pMatView	= m_pEffect->GetParam("u_MatView");
	m_pMatProj	= m_pEffect->GetParam("u_MatProj");

	m_pTexture	= m_pEffect->GetParam("u_Texture");
	m_pUVCellCnt= m_pEffect->GetParam("u_CellSize");
	m_pUVTable	= m_pEffect->GetParam("u_UVTable");
	m_pColors	= m_pEffect->GetParam("u_Colors");
	m_pSizes	= m_pEffect->GetParam("u_Sizes");
	m_pKeyLines	= m_pEffect->GetParam("u_KeyLines");
}

void RenderBase::DeinitParameter()
{
	if(m_pCameraX)	m_pCameraX->Release();
	if(m_pCameraY)	m_pCameraY->Release();


	if(m_pMatView)	m_pMatView->Release();
	if(m_pMatProj)	m_pMatProj->Release();

	if(m_pTexture)	m_pTexture->Release();
	if(m_pUVCellCnt)m_pUVCellCnt->Release();
	if(m_pUVTable)	m_pUVTable->Release();

	if(m_pKeyLines)	m_pKeyLines->Release();
	if(m_pColors)	m_pColors->Release();
	if(m_pSizes)	m_pSizes->Release();
}
