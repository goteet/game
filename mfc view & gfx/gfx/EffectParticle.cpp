#include "stdafx.h"
#include "EffectParticle.h"


void EffectParticle::OnDestroy()
{
	if(m_pMatWorld)	m_pMatWorld->Release();
}

void EffectParticle::GetParams()
{
	m_pMatWorld	= GetParam("u_MatWorld");
}

const char* EffectParticle::GetVertexShader()
{
	static const char* s_szVertexShader = {
		"#version 110\n"
		"uniform vec4 u_CameraX;"  
		"uniform vec4 u_CameraY;"
		"uniform mat4 u_MatWorld;"
		"uniform mat4 u_MatView;"  
		"uniform mat4 u_MatProj;"
		"uniform vec3 u_CellSize;"
		"uniform vec2 u_KeyLines;"
		"uniform vec4 u_Colors[8];"
		"uniform vec2 u_Sizes[8];"		
		"uniform vec2 u_UVTable[16];"
		"attribute vec4 in_Coord_UV;"
		"attribute vec3 in_Position;"
		"attribute vec3 in_DynRotate;"
		"varying vec2 vTexUV;"
		"varying vec4 vColor;"
		"vec2 GetUV(vec2 uv, float life){"
		"	life *= u_CellSize.z;"
		"	int index = int(floor(life));"
		"	return uv * u_CellSize.yx + u_UVTable[index];"
		"}"
		"vec4 GetColor(float life){"
		"	life *= u_KeyLines.x;"
		"	int prevKey = int(floor(life));"
		"	float p = fract(life);"
		"	return mix(	u_Colors[prevKey],"
		"				u_Colors[prevKey+1],"
		"				p);"
		"}"
		"vec2 GetSize(float life){"
		"	life *= u_KeyLines.y;"
		"	int prevKey = int(floor(life));"
		"	float p = fract(life);"
		"	return in_Coord_UV.xy * mix(u_Sizes[prevKey], u_Sizes[prevKey+1], p);"
		"}"
		"vec3 GetBillBoardExt(float rotate, float life){"
		"	vec3 axisP	= u_CameraX.xyz;"
		"	vec3 axisQ	= u_CameraY.xyz;"
		"	float cosR	= cos(rotate);"
		"	float sinR	= sin(rotate);"
		"	vec3 newP	= axisP * cosR + axisQ * sinR;"
		"	vec3 newQ	= axisP * sinR - axisQ * cosR;"
		"	vec2 size	= GetSize(life);"
		"	vec3 ext	= newP * size.x + newQ * size.y;"
		"	return ext;"
		"}"
		"void main(){"
		"	float life	= in_DynRotate.y;"
		"	vec3 posExt	= GetBillBoardExt(in_DynRotate.x, life);"
		"	vec4 posWorld	= u_MatWorld* vec4(in_Position,1.0) + vec4(posExt, 0.0);"
		"	vTexUV		= GetUV(in_Coord_UV.zw, life);"
		"	vColor		= GetColor(life);"
		"	gl_Position	= u_MatProj * u_MatView * posWorld;"
		"}\0\0"
	};
	return s_szVertexShader;
}

const char* EffectParticle::GetPixelShader()
{
	static const char* s_szFragShader = {
		"#version 110\n"   
		"uniform sampler2D u_Texture;"
		"varying vec2 vTexUV;"
		"varying vec4 vColor;"
		"void main(){"
		"   gl_FragColor = vColor * texture2D(u_Texture, vTexUV);"
		"}\0\0"  
	};
	return s_szFragShader;
}

graphic::AttribLoc* EffectParticle::GetAttribLoc()
{
	static graphic::AttribLoc loc[] =
	{		
		{0,	"in_Coord_UV"},
		{1,	"in_Position"},
		{2,	"in_DynRotate"},
	};
	return loc;
}

int EffectParticle::GetAttribLocCount()
{
	return 3;
}




















