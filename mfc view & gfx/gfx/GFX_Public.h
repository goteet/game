#ifndef GFX_PUBLIC_INCLUDE_H
#define GFX_PUBLIC_INCLUDE_H

#include "BaseInstance.h"

namespace graphic{
	class Device;
}

void GFXInitial(graphic::Device* device);
void GFXDeinit();
void GFXRender(float* matrixView, float* matrixProj);

BaseInstance* CreateParticle();
BaseInstance* CreateSpark();
#endif