#ifndef GFX_RENDER_BASE_H
#define GFX_RENDER_BASE_H

class Material;
class TimeLine;

namespace graphic{
	class Device;
	class ShaderParam;
	class IShaderEffect;
}
class RenderBase
{
public:
	virtual ~RenderBase(){}
	bool Init(graphic::Device* device);
	void Deinit();
	void Render(float* matView, float* matProj);

protected:
	virtual void DoRender(float* matView, float* matProj) = 0;
	virtual bool InitRender() = 0;
	virtual void DeinitRender() = 0;

protected:
	void SetTimeLine(TimeLine* timeline);
	void SetMatertial(Material* matertial);
	
	
	graphic::Device*		m_pDevice;
	graphic::IShaderEffect*	m_pEffect;

private:
	void InitParameter();
	void DeinitParameter();
	void SetCameraParam( float* matView );

	graphic::ShaderParam* m_pCameraX;
	graphic::ShaderParam* m_pCameraY;

	graphic::ShaderParam* m_pMatView;
	graphic::ShaderParam* m_pMatProj;

	graphic::ShaderParam* m_pTexture;
	graphic::ShaderParam* m_pUVCellCnt;
	graphic::ShaderParam* m_pUVTable;
	graphic::ShaderParam* m_pColors;
	graphic::ShaderParam* m_pSizes;
	graphic::ShaderParam* m_pKeyLines;

};

#endif