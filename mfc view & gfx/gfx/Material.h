#ifndef GFX_MATERIAL_H
#define GFX_MATERIAL_H

#include <string>
#include "math_inc.h"
class Material
{
public:
	Material();
	std::string m_Tex;

	bool SetSize(int row, int col);	
	float*	GetUVTable();
	int		GetUVCellCount();
	float*	GetUVCell();
private:
	int m_Col, m_Row;
	int m_Count;
	Math::vec3 m_CellCnt;
	void RebuildUVTable();

	enum { MAX_CELL = 24};
	Math::vec2 m_UVTable[MAX_CELL];
};



#endif