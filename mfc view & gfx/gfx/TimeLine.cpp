#include "stdafx.h"
#include "TimeLine.h"

TimeLine::TimeLine()
{
	Math::vec4 DEF_COLOR = Math::vec4(1,1,1,1);
	Math::vec2 DEF_SIZE = Math::vec2(0.2,0.2);
	for(int i = 0; i != MAX_KEY_NODE; i++)
	{
		m_Colors.SetKey(i, DEF_COLOR);
		m_Sizes.SetKey(i, DEF_SIZE);
	}
	m_Colors.SetCount(8);
	m_Sizes.SetCount(8);


	m_Colors.SetKey(0, Math::vec4(1,1,1,0));
	m_Colors.SetKey(1, Math::vec4(1,0,0,1));

	m_Colors.SetKey(2, Math::vec4(0,1,0,1));
	m_Colors.SetKey(3, Math::vec4(0,0,1,1));
	m_Colors.SetKey(4, Math::vec4(0,1,1,1));

	m_Colors.SetKey(5, Math::vec4(1,1,0,1));
	m_Colors.SetKey(6, Math::vec4(1,0,1,1));
	m_Colors.SetKey(7, Math::vec4(1,1,1,0));


/*
	m_Sizes.SetKey(0, Math::vec2(0.3,0.3));
	m_Sizes.SetKey(1, Math::vec2(0.3,0.3));

	m_Sizes.SetKey(2, Math::vec2(0.3,0.3));
	m_Sizes.SetKey(3, Math::vec2(0.3,0.3));
	m_Sizes.SetKey(4, Math::vec2(0.3,0.3));

	m_Sizes.SetKey(5, Math::vec2(0.3,0.3));
	m_Sizes.SetKey(6, Math::vec2(0.3,0.3));
	m_Sizes.SetKey(7, Math::vec2(0,0));
	*/
}

float* TimeLine::GetColorPtr()
{
	return m_Colors.GetPTR();
}

float* TimeLine::GetSizePtr()
{
	return m_Sizes.GetPTR();
}

KeyLine<Math::vec4>*	TimeLine::GetColor()
{
	return &m_Colors;
}

KeyLine<Math::vec2>*	TimeLine::GetSize()
{
	return &m_Sizes;
}
