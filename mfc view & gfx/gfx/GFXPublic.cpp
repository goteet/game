#include "stdafx.h"
#include "GFX_Public.h"
#include "RenderParticle.h"
#include "RenderSpark.h"
#include "Particle.h"
#include "Spark.h"


void GFXInitial(graphic::Device* device)
{
	RenderParticle::instance()->Init(device);
	RenderSpark::instance()->Init(device);
}
void GFXDeinit()
{
	RenderSpark::instance()->Deinit();
	RenderParticle::instance()->Deinit();
}
void GFXRender(float* matrixView, float* matrixProj)
{
	RenderSpark::instance()->Render(matrixView, matrixProj);
	RenderParticle::instance()->Render(matrixView, matrixProj);
}

BaseInstance* CreateParticle()
{
	return new ParticleInstance();
}

BaseInstance* CreateSpark()
{
	return new SparkInstance();
}