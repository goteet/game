#ifndef GFX_RENDER_SPARK_H
#define GFX_RENDER_SPARK_H

#include <vector>
#include "device.h"
#include "RenderBase.h"
#include "EffectSpark.h"

class SparkInstance;
class Spark;
class Material;
class TimeLine;
namespace graphic{
	class VertexBuffer;
	class IndexBuffer;
	class VertexDecl;
}
class RenderSpark : public RenderBase
{
public:
	static RenderSpark* instance();	
	void Push(SparkInstance* data);

protected:
	bool InitRender();
	void DeinitRender();
	void DoRender(float* matView, float* matProj);

private:
	enum{ SPARK_SIZE=4096 };

	graphic::VertexBuffer*	m_pVBSparkFix;
	graphic::VertexBuffer*	m_pVBSparkRnd[5];
	graphic::IndexBuffer*	m_pIBSpark;
	graphic::VertexDecl*	m_pDecl;

	
	void InitVBSpark();
	void InitIBSpark();
	void InitDeclSpark();
	
	std::vector<SparkInstance*> m_vecSparks;
};
#endif 

