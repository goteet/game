#include "stdafx.h"
#include "Spark.h"


BEGIN_DATA_ENTRY(SparkInstance)
	DEF_DATA_ENTRY(SparkInstance, m_SparkCnt, int)
	DEF_DATA_ENTRY(SparkInstance, m_Position, Math::vec3)
	DEF_DATA_ENTRY(SparkInstance, m_GenRange, Math::vec3)
	DEF_DATA_ENTRY(SparkInstance, m_Velocity, Math::vec4)
	DEF_DATA_ENTRY(SparkInstance, m_Acceleration, Math::vec4)
	DEF_DATA_ENTRY(SparkInstance, m_Radial, Math::vec2)
	DEF_DATA_ENTRY(SparkInstance, m_LifeShift, float)
	DEF_DATA_ENTRY(SparkInstance, m_MaxLife, float)
	DEF_DATA_ENTRY(SparkInstance, m_bAutoReborn, int)
	DEF_DATA_ENTRY(SparkInstance, m_bRebornTime, float)	
END_DATA_ENTRY

#include "RandFunc.h"

void SparkInstance::Update(float dTime)
{
	m_CurLife += dTime;

	if( IsDead() && m_bAutoReborn && m_CurLife >= m_bRebornTime + m_MaxLife)
	{
		Reborn();
	}
}

SparkInstance::SparkInstance()
	: m_Position(0, 0, -20)
	, m_MaxLife(3.5)
	, m_LifeShift(3.5)
	, m_SparkCnt(512)
	, m_GenRange(10,10,10)
	, m_Velocity(0,0,0,0)
	, m_Acceleration(0,0,0,0)
	, m_Radial(10,0)
	, m_bAutoReborn(1)
	, m_bRebornTime(1.0f)
	, m_CurLife(0)
{


	InitSeed();
	m_Material.m_Tex = "../res/fire.png";
	m_Material.SetSize(2,4);
#if 1
	m_Material.m_Tex = "../res/cnt.bmp";
	m_Material.SetSize(2,2);
#endif
}

Material* SparkInstance::GetMaterial()
{
	return &m_Material;
}

TimeLine* SparkInstance::GetTimeLine()
{
	return &m_TimeLine;
}

bool SparkInstance::IsDead()
{
	return (m_CurLife >= m_MaxLife);
}

float SparkInstance::GetLife()
{
	return min(m_CurLife / m_MaxLife, 1.0f);
}

int SparkInstance::GetSeed()
{
	return m_RndSeed;
}

void SparkInstance::InitSeed()
{
	m_RndSeed = rand() % 5;
}

#include "RenderSpark.h"
void SparkInstance::AddToRender()
{
	RenderSpark::instance()->Push(this);
}

void SparkInstance::Reborn()
{
	m_CurLife = 0.0f;//do nothing;
	InitSeed();
}

void SparkInstance::Release()
{
	delete this;
}
