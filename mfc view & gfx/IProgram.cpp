#include "stdafx.h"
#include "IProgram.h"

#include "balloon/BalloonEx2.h"
#include "balloon/BalloonManager.h"
#include "balloon/RenderBalloon.h"

#include <fstream>
#include <string>
using namespace std;

class Prog : public IProgram
{
	virtual void Init();
	virtual void Deinit();
	virtual void RenderScene();
	virtual void Release();
	virtual void OnResize(int width, int height);
	virtual void OnCommand(int ID, int arg1, int arg2);

	virtual void OnMouseInput(int message, int flag, int x, int y, int d);
	virtual void OnLButtonUp( int nFlags, int x, int y );
	virtual void OnLButtonDown( int nFlags, int x, int y );
	virtual void OnMouseMove( int nFlags, int x, int y );
	virtual void OnMouseWheel(int nFlags, short zDelta, int x, int y);

public:
	Prog();

	virtual void ShowBalloon(int arg1, int arg2, char* url);
	void OnShowBalloon(int x, int y){}


public:
	Graphic::Device* m_pDevice;

	BalloonEx2* balloon;

	int m_width;
	int m_height;
};

IProgram* IProgram::Create()
{
	return new Prog();
}








Prog::Prog()
	: balloon(0)
	, m_pDevice(0)
{
	
}


void Prog::Release()
{
	delete this;
}

void Prog::RenderScene()
{
	BalloonManager::instance()->Tick();
	BalloonManager::instance()->Render();
}

void Prog::Init()
{
	glEnable(GL_TEXTURE_2D);
	WebViewInit();
	m_pDevice = Graphic::CreateDevice();


	RenderBalloon::instance()->Init(m_pDevice);
	RenderBalloon::instance()->SetScreenSize(800,600);

	balloon = new BalloonEx2();
}

void Prog::OnResize( int width, int height )
{
	m_width = width;
	m_height = height;
	const float ar = (float) width / (float) height;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	const float PI = 3.1415926f;
	gluPerspective(45, ar, 0.1f, 5000.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity() ;


	RenderBalloon::instance()->SetScreenSize(width,height);
	BalloonManager::instance()->Resize();
}

void Prog::OnCommand( int ID, int arg1, int arg2)
{
	switch(ID)
	{
	case SHOW_BALLOON:
		OnShowBalloon(arg1, arg2);
		break;
	}
}

void Prog::ShowBalloon(int x, int y, char* url)
{	
	balloon->SetSize(200,200, 0);
	balloon->SetAutoResize();
	balloon->ShowLink(x, y, (char*)url);
}

void Prog::Deinit()
{
	delete balloon;
}

void Prog::OnLButtonUp( int nFlags, int x, int y )
{
	BalloonManager::instance()->OnLMouseUp(x,y);
			
}

void Prog::OnLButtonDown( int nFlags, int x, int y )
{
	BalloonManager::instance()->OnLMouseDown(x,y);
}

void Prog::OnMouseMove( int nFlags, int x, int y )
{
	BalloonManager::instance()->OnMouseMove(x, y);
}

void Prog::OnMouseWheel( int nFlags, short zDelta, int x, int y )
{
	BalloonManager::instance()->OnMouseWheel(zDelta, x, y);
}

void Prog::OnMouseInput(int message, int flag, int x, int y, int d)
{
	switch(message)
	{
	case WM_LBUTTONDOWN:	BalloonManager::instance()->OnLMouseDown(x,y);	break;
	case WM_LBUTTONUP:		BalloonManager::instance()->OnLMouseUp(x,y);	break;
	case WM_MOUSEMOVE:		BalloonManager::instance()->OnMouseMove(x, y);	break;

	}
}