#pragma once

#include "IProgram.h"
#include "Camera.h"
#include "gfx/GFX_Public.h"
class GFXProg : public IProgram
{
public:
	GFXProg();
	virtual void Init(graphic::Device* _pDevice);
	virtual void Deinit();
	virtual void RenderScene();
	virtual void Release();

	virtual void OnResize(int width, int height);
	virtual void OnCommand(int ID, int arg1, int arg2){}
	virtual void* GetCommand(int ID, int arg1, int arg2){return 0;}


public:
	virtual void OnLStartDragging(int x, int y,UINT flag);
	virtual void OnLStopDragging(int x, int y,UINT flag);
	virtual void OnLDragging(int x, int y,UINT flag);

	virtual void OnRStartDragging(int x, int y,UINT flag);
	virtual void OnRStopDragging(int x, int y,UINT flag);
	virtual void OnRDragging(int x, int y,UINT flag);

	virtual void OnMouseWheel(int length, UINT flag);

private:
	void UpdateCamera();
	Camera	m_Camera;
	Camera2 m_Camera2;
	int		m_DownLPos[2];
	int		m_DownRPos[2];
	int		m_DragRPos[2];

	float	DragAngle;
	float	OldAngle;
	bool	m_bMouseDownR;

	void InitParticles();
	void DeinitParticles();
	void UpdateParticles();
	static const int PTX_CNT = 400;
	BaseInstance* particle[PTX_CNT];
	BaseInstance* spark[PTX_CNT];

	void InitFire();
	void DeinitFire();
	void UpdateFire();
	BaseInstance* m_pFire;
	BaseInstance* m_pSmoke;
	BaseInstance* m_pWater;
	BaseInstance* m_pWater2;


	Graphic::Device* m_pDevice;
	graphic::Device* m_pDevice2;

	int		m_ScreenSize[4];//half
	int		m_ScreenPos[2];//mid

	

};