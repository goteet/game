#include "stdafx.h"
#include"GFXProg.h"
#include "gfx/RenderGrid.h"
#include "globe/Globe.h"
#include "globe/RenderGlobe.h"
GFXProg::GFXProg()
: OldAngle(0.0f)
, m_bMouseDownR(false)
{
}

void GFXProg::Init(graphic::Device* _pDevice)
{
	//Globe::instance()->Init();
	m_pDevice2 = _pDevice;
	m_pDevice = Graphic::CreateDevice();
	RenderGrid::instance()->Init(m_pDevice2);
	//RenderGlobe::instance()->Init(m_pDevice);

	GFXInitial(m_pDevice2);
	InitParticles();
	InitFire();


	//Globe::instance()->UpdateForward(m_Camera2);
	//Globe::instance()->UpdateDistance(m_Camera2);

}

void GFXProg::Deinit()
{
	//Globe::instance()->Deinit();
	//RenderGlobe::instance()->Deinit();
	RenderGrid::instance()->Deinit();
	
	DeinitParticles();
	DeinitFire();
	GFXDeinit();
}

void GFXProg::RenderScene()
{
	UpdateCamera();

	Math::mat4* matView = m_Camera2.GetViewMatrix();
	Math::mat4* matProj = m_Camera.GetProjMatrix();
	UpdateParticles();
	//UpdateFire();

	//Globe::instance()->AddToRender();
	//RenderGlobe::instance()->Render(matView->mp, matProj->mp);
	RenderGrid::instance()->Render(m_Camera.GetViewMatrix()->mp, m_Camera.GetProjMatrix()->mp);
	GFXRender(m_Camera.GetViewMatrix()->mp, m_Camera.GetProjMatrix()->mp);

}

void GFXProg::Release()
{
	//delete m_pDevice;
	delete this;
}

void GFXProg::OnLStartDragging(int x, int y,UINT flag)
{
	//HideCursor();
	m_DownLPos[0] = x;
	m_DownLPos[1] = y;


	int midx = GetWidth() / 2;
	int midy = GetHeight() / 2;
	SetCursor(midx,midy);
}

void GFXProg::OnLStopDragging(int x, int y,UINT flag)
{
	OldAngle += DragAngle;
	ShowCursor(); 
	SetCursor(m_DownLPos[0],m_DownLPos[1]);
}

void GFXProg::OnLDragging(int x, int y,UINT flag)
{
	int midx = GetWidth() / 2;
	int midy = GetHeight() / 2;
	const float SENSITIVE_X = 10.0f;
	const float SENSITIVE_Y = 10.0f;
	int newX = (midx-x);
	int newY = (y-midy);
	SetCursor(midx,midy);
	if(newX != 0)	m_Camera.SetYaw(m_Camera.aYaw + newX/SENSITIVE_X);
	if(newY != 0) m_Camera.SetPitch(m_Camera.aPitch+newY/SENSITIVE_Y);
	float dx = newX != 0 ? newX/100.0f : 0;
	float dy = newY != 0 ? newY/100.0f : 0;
	m_Camera2.OnMove(dx,dy);
	m_Camera2.BuildViewMatrix();
	m_Camera.BuildViewMatrix();

	//Globe::instance()->UpdateForward(m_Camera2);
}

void GFXProg::OnRStartDragging(int x, int y,UINT flag)
{
	m_bMouseDownR = true;
	m_DragRPos[0] = x;
	m_DragRPos[1] = y;
}

void GFXProg::OnRStopDragging(int x, int y,UINT flag)
{
	m_bMouseDownR = false;
	//ShowCursor(); 
	//SetCursor(m_DownRPos[0],m_DownRPos[1]);
}

void GFXProg::OnRDragging(int x, int y,UINT flag)
{
	m_DragRPos[0] = x;
	m_DragRPos[1] = y;
}

void GFXProg::OnMouseWheel( int length, UINT flag )
{
	const float SENSITIVE = 120.0f;
	float l = length / SENSITIVE;
	m_Camera.SetHeight(-l);
	m_Camera2.OnRoll(-l);
	
	//Globe::instance()->UpdateDistance(m_Camera2);
}

void GFXProg::OnResize( int width, int height )
{
	m_Camera2.BuidProjMatrx(width, height);
	m_Camera.BuidProjMatrx(width, height);
	m_ScreenSize[2] = width / 2;
	m_ScreenSize[3] = height / 2;
}

void GFXProg::InitParticles()
{

	for(int i = 0; i != PTX_CNT; i++)
	{

		/*particle
		DEF_DATA_ENTRY(ParticleInstance, m_Position, Math::vec3)
		DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_Type, int)
		DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_EmittRange, Math::vec4)
		DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_AccFix, Math::vec3)
		DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_AccRnd, Math::vec3)
		DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_VelFix, Math::vec3)
		DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_VelRadial, Math::vec3)
		DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_VelRnd, Math::vec3)
		DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_RotFix, float)
		DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_RotRnd, float)
		DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_LifeRange, Math::vec2)
		DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_GenInit, int)
		DEF_DATA_ENTRY(ParticleInstance, m_Emitter.m_GenPerSec, float)	
		*/
		particle[i] = CreateParticle();
		Variant v = Math::vec3(i / 20 * 20.0f,0, -i % 20 * 20.0f);
		particle[i]->SetParam("m_Position", v);

		v = rand() % 3;
		particle[i]->SetParam("m_Emitter.m_Type", v);

		v = Math::vec3(0,0,0);
		particle[i]->SetParam("m_Emitter.m_VelFix", v);
		particle[i]->SetParam("m_Emitter.m_VelRnd", v);
		particle[i]->SetParam("m_Emitter.m_AccFix", v);
		particle[i]->SetParam("m_Emitter.m_AccRnd", v);
		particle[i]->SetParam("m_Emitter.m_VelRadial", v);

		v = Math::vec3(1,1,1);
		particle[i]->SetParam("m_Emitter.m_VelRnd", v);
		v = Math::vec3(0,1,0);
		particle[i]->SetParam("m_Emitter.m_VelFix", v);

		v = Math::vec3(1,1,1);
		particle[i]->SetParam("m_Emitter.m_VelRnd", v);
		v = Math::vec3(0,1,0);
		particle[i]->SetParam("m_Emitter.m_VelFix", v);

		v = 256;
		particle[i]->SetParam("m_Emitter.m_GenPerSec", v);



		/*
		DEF_DATA_ENTRY(SparkInstance, m_SparkCnt, int)
		DEF_DATA_ENTRY(SparkInstance, m_Position, Math::vec3)
		DEF_DATA_ENTRY(SparkInstance, m_GenRange, Math::vec3)
		DEF_DATA_ENTRY(SparkInstance, m_Velocity, Math::vec4)
		DEF_DATA_ENTRY(SparkInstance, m_Acceleration, Math::vec4)
		DEF_DATA_ENTRY(SparkInstance, m_Radial, Math::vec2)
		DEF_DATA_ENTRY(SparkInstance, m_LifeShift, float)
		DEF_DATA_ENTRY(SparkInstance, m_MaxLife, float)
		DEF_DATA_ENTRY(SparkInstance, m_bAutoReborn, int)
		DEF_DATA_ENTRY(SparkInstance, m_bRebornTime, float)	
		*/
		spark[i] = CreateSpark();
		v = Math::vec3(i / 20 * 20.0f  + 10 ,0, -i % 20 * 20.0f + 10.0f);
		spark[i]->SetParam("m_Position", v);

		v = Math::vec3(1,1,1);
		spark[i]->SetParam("m_GenRange", v);

		v = 256;
		spark[i]->SetParam("m_SparkCnt", v);

		v = Math::vec4(2,2,2, 0);
		spark[i]->SetParam("m_Velocity", v);
		v = Math::vec4(0, 0, 0, 0);
		spark[i]->SetParam("m_Acceleration", v);

		v = Math::vec2(5,-5);
		spark[i]->SetParam("m_Radial", v);


		v = 0.3f;
		spark[i]->SetParam("m_LifeShift", v);
		

	}
}

void GFXProg::DeinitParticles()
{
	for(int i = 0; i != PTX_CNT; i++)
	{
		particle[i]->Release();
		spark[i]->Release();
	}
}

void GFXProg::UpdateParticles()
{
	//particles
	if(1)
	{
		for(int i = 0; i != PTX_CNT; i++)
		{
			particle[i]->Update(0.05f);
			particle[i]->AddToRender();
		}
	}

	//spark
	if(1)
	{
		for(int i = 0; i != PTX_CNT; i++)
		{
			spark[i]->Update(0.05f + rand() * 0.05f / RAND_MAX );
			if(spark[i]->IsDead() == false)	spark[i]->AddToRender();
		}
	}
}

void GFXProg::UpdateCamera()
{
	if(m_bMouseDownR)	
	{

		const float SENSITIVE_H = -0.001f;
		const float SENSITIVE_V = 0.001f;

		float newX = (m_DragRPos[0]-m_ScreenSize[2]) * SENSITIVE_H;
		float newY = (m_DragRPos[1]-m_ScreenSize[3]) * SENSITIVE_V;

		if(newX<0)		m_Camera.MoveLeft(-newX);
		else if(newX>0)	m_Camera.MoveRight(newX);
		if(newY<0)		m_Camera.MoveForward(-newY);
		else if(newY>0)	m_Camera.MoveBackward(newY);
	}

	m_Camera.BuildViewMatrix();
	m_Camera2.BuildViewMatrix();
}

void GFXProg::InitFire()
{
	m_pFire = CreateParticle();
	m_pSmoke = CreateParticle();
	m_pWater = CreateParticle();
	m_pWater2 = CreateParticle();





	//position
	Variant v = Math::vec3(0, 10, -20);
	m_pFire->SetParam("m_Position", v);
	m_pSmoke->SetParam("m_Position", v);
	v = Math::vec3(18, 0, -20);
	m_pWater->SetParam("m_Position", v);
	m_pWater2->SetParam("m_Position", v);

	//material
	m_pFire->GetMaterial()->m_Tex = "../res/gfx/fire.png";
	m_pFire->GetMaterial()->SetSize(2,4);

	m_pSmoke->GetMaterial()->m_Tex = "../res/gfx/smoke.png";
	m_pWater->GetMaterial()->m_Tex = "../res/gfx/water2.png";
	m_pWater2->GetMaterial()->m_Tex = "../res/gfx/smoke.png";
	/*
	m_pWater->GetMaterial()->m_Tex = "../res/gfx/water.png";
	m_pWater->GetMaterial()->SetSize(4,4);
	*/

	//keyframe size
	m_pFire->GetTimeLine()->GetSize()->SetKey(0,Math::vec2(5,8));
	m_pSmoke->GetTimeLine()->GetSize()->SetKey(0,Math::vec2(7,7));
	m_pWater->GetTimeLine()->GetSize()->SetKey(0,Math::vec2(0.75,0.75));
	m_pWater2->GetTimeLine()->GetSize()->SetKey(0,Math::vec2(0.75,0.75));
	m_pWater2->GetTimeLine()->GetSize()->SetKey(0,Math::vec2(0.5,0.5));

	m_pFire->GetTimeLine()->GetSize()->SetCount(1);
	m_pSmoke->GetTimeLine()->GetSize()->SetCount(1);
	m_pWater->GetTimeLine()->GetSize()->SetCount(1);
	m_pWater2->GetTimeLine()->GetSize()->SetCount(1);

	//keyframe color
	m_pFire->GetTimeLine()->GetColor()->SetKey(0,Math::vec4(1,1,1,1));
	m_pSmoke->GetTimeLine()->GetColor()->SetKey(0,Math::vec4(0.5f,0.5f,0.5f,0.2f));
	m_pWater->GetTimeLine()->GetColor()->SetKey(0,Math::vec4(0.35f,0.6f,1.0f,0.75f));
	m_pWater->GetTimeLine()->GetColor()->SetKey(1,Math::vec4(0.35f,0.6f,1.0f,1.0f));
	m_pWater->GetTimeLine()->GetColor()->SetKey(2,Math::vec4(0.35f,0.6f,1.0f,0.0f));
	m_pWater2->GetTimeLine()->GetColor()->SetKey(0,Math::vec4(0.35f,0.6f,1.0f,1.0f));
	m_pWater2->GetTimeLine()->GetColor()->SetKey(1,Math::vec4(0.35f,0.6f,1.0f,1.0f));
	m_pWater2->GetTimeLine()->GetColor()->SetKey(2,Math::vec4(0.35f,0.6f,1.0f,1.0f));
	
	m_pFire->GetTimeLine()->GetColor()->SetCount(1);
	m_pSmoke->GetTimeLine()->GetColor()->SetCount(1);
	m_pWater->GetTimeLine()->GetColor()->SetCount(3);
	m_pWater2->GetTimeLine()->GetColor()->SetCount(3);

	//emitt type
	v = 0;
	m_pFire->SetParam("m_Emitter.m_Type", v);
	v = 2;
	m_pSmoke->SetParam("m_Emitter.m_Type", v);
	v = 0;
	m_pWater->SetParam("m_Emitter.m_Type", v);
	m_pWater2->SetParam("m_Emitter.m_Type", v);

	//speed
	v = Math::vec3(0,0,0);
	m_pFire->SetParam("m_Emitter.m_VelFix", v);
	m_pFire->SetParam("m_Emitter.m_VelRnd", v);
	m_pFire->SetParam("m_Emitter.m_AccFix", v);
	m_pFire->SetParam("m_Emitter.m_AccRnd", v);
	m_pFire->SetParam("m_Emitter.m_VelRadial", v);

	m_pSmoke->SetParam("m_Emitter.m_VelRadial", v);

	m_pWater->SetParam("m_Emitter.m_VelRadial", v);
	m_pWater2->SetParam("m_Emitter.m_VelRadial", v);

	v = Math::vec3(0,1,0);
	m_pSmoke->SetParam("m_Emitter.m_VelFix", v);
	m_pSmoke->SetParam("m_Emitter.m_AccFix", v);
	m_pSmoke->SetParam("m_Emitter.m_AccRnd", v);
	v = Math::vec3(0.5f,1,0.5f);
	m_pSmoke->SetParam("m_Emitter.m_VelRnd", v);

	v = Math::vec3(0.0f, -2, 0.0f);
	m_pWater->SetParam("m_Emitter.m_AccFix", v);
	m_pWater2->SetParam("m_Emitter.m_AccFix", v);
	v = Math::vec3(0.2f, 0.2f, 0.2f);
	m_pWater->SetParam("m_Emitter.m_AccRnd", v);
	m_pWater2->SetParam("m_Emitter.m_AccRnd", v);
	v = Math::vec3(-5, 6, 0.0f);
	m_pWater->SetParam("m_Emitter.m_VelFix", v);
	m_pWater2->SetParam("m_Emitter.m_VelFix", v);
	v = Math::vec3(0.1f, 0.1f, 0.1f);
	m_pWater->SetParam("m_Emitter.m_VelRnd", v);
	m_pWater2->SetParam("m_Emitter.m_VelRnd", v);


	//rotation
	v = Math::PI_4 / 4.0f;
	m_pSmoke->SetParam("m_Emitter.m_RotFix", v);
	m_pSmoke->SetParam("m_Emitter.m_RotRnd", v);
	

	//generator cnt
	v = 16.0f;
	m_pFire->SetParam("m_Emitter.m_GenPerSec", v);
	v = 128.0f;
	m_pWater->SetParam("m_Emitter.m_GenPerSec", v);
	v = 32.0f;
	m_pWater2->SetParam("m_Emitter.m_GenPerSec", v);
	v = 2.0f;
	m_pSmoke->SetParam("m_Emitter.m_GenPerSec", v);
	
	v = Math::vec4(1,1,1,0);
	m_pFire->SetParam("m_Emitter.m_EmittRange", v);
	v = Math::vec4(0.2f,0,0.2f,0);
	m_pWater->SetParam("m_Emitter.m_EmittRange", v);
	m_pWater2->SetParam("m_Emitter.m_EmittRange", v);
	v = Math::vec4(3,2,3,0);
	m_pSmoke->SetParam("m_Emitter.m_EmittRange", v);

	v = 10;
	m_pFire->SetParam("m_Emitter.m_GenInit", v);
	
	v = 0;
	m_pWater->SetParam("m_Emitter.m_GenInit", v);
	m_pWater2->SetParam("m_Emitter.m_GenInit", v);
	m_pSmoke->SetParam("m_Emitter.m_GenInit", v);


	//life
	v = Math::vec2(1,2);
	m_pFire->SetParam("m_Emitter.m_LifeRange", v);
	v = Math::vec2(2,4.5);
	m_pWater->SetParam("m_Emitter.m_LifeRange", v);
	m_pWater2->SetParam("m_Emitter.m_LifeRange", v);

	v = Math::vec2(5, 7);
	m_pSmoke->SetParam("m_Emitter.m_LifeRange", v);

	
}

void GFXProg::DeinitFire()
{
	m_pFire->Release();
	m_pSmoke->Release();
	m_pWater->Release();
	m_pWater2->Release();
}

void GFXProg::UpdateFire()
{

	m_pFire->Update(0.05f);
	m_pSmoke->Update(0.05f);
	m_pWater->Update(0.05f);
	m_pWater2->Update(0.05f);

	m_pFire->AddToRender();
	m_pSmoke->AddToRender();
	m_pWater->AddToRender();
	m_pWater2->AddToRender();
}
