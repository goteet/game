/*
* Copyright (c) 2007-2009 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

#include <Box2D/Common/b2Math.h>

/// Solve A * x = b, where b is a column vector. This is more efficient
/// than computing the inverse in one-shot cases.
math::vec3 b2Mat33::Solve33(const math::vec3& b) const
{
	float32 det = dot(line[0], cross(line[1], line[2]));
	if (det != 0.0f)
	{
		det = 1.0f / det;
	}
	math::vec3 x;
	x.x = det * dot(b, cross(line[1], line[2]));
	x.y = det * dot(line[0], cross(b, line[2]));
	x.z = det * dot(line[0], cross(line[1], b));
	return x;
}

/// Solve A * x = b, where b is a column vector. This is more efficient
/// than computing the inverse in one-shot cases.
math::vec2 b2Mat33::Solve22(const math::vec2& b) const
{
	float32 a11 = line[0].x, a12 = line[1].x, a21 = line[0].y, a22 = line[1].y;
	float32 det = a11 * a22 - a12 * a21;
	if (det != 0.0f)
	{
		det = 1.0f / det;
	}
	math::vec2 x;
	x.x = det * (a22 * b.x - a12 * b.y);
	x.y = det * (a11 * b.y - a21 * b.x);
	return x;
}

///
void b2Mat33::GetInverse22(b2Mat33* M) const
{
	float32 a = line[0].x, b = line[1].x, c = line[0].y, d = line[1].y;
	float32 det = a * d - b * c;
	if (det != 0.0f)
	{
		det = 1.0f / det;
	}

	M->line[0].x =  det * d;	M->line[1].x = -det * b; M->line[0].z = 0.0f;
	M->line[0].y = -det * c;	M->line[1].y =  det * a; M->line[1].z = 0.0f;
	M->line[2].x = 0.0f; M->line[2].y = 0.0f; M->line[2].z = 0.0f;
}

/// Returns the zero matrix if singular.
void b2Mat33::GetSymInverse33(b2Mat33* M) const
{
	float32 det = dot(line[0], cross(line[1], line[2]));
	if (det != 0.0f)
	{
		det = 1.0f / det;
	}

	float32 a11 = line[0].x, a12 = line[1].x, a13 = line[2].x;
	float32 a22 = line[1].y, a23 = line[2].y;
	float32 a33 = line[2].z;

	M->line[0].x = det * (a22 * a33 - a23 * a23);
	M->line[0].y = det * (a13 * a23 - a12 * a33);
	M->line[0].z = det * (a12 * a23 - a13 * a22);

	M->line[1].x = M->line[0].y;
	M->line[1].y = det * (a11 * a33 - a13 * a13);
	M->line[1].z = det * (a13 * a12 - a11 * a23);

	M->line[2].x = M->line[0].z;
	M->line[2].y = M->line[1].z;
	M->line[2].z = det * (a11 * a22 - a12 * a12);
}
